﻿Thank for checking out Pirate Rogue b02!

Default Controls:
--

Rotate Ship Left = A
Rotate Ship Right = D
Rotate Sails Left = Q
Rotate Sails Right = E
Set Sails Up = W
Set Sails Down = S
Zoom In = R
Zoom Out = F
Debug Mode = F1

Feel free to change them in options menu.

--
https://www.reddit.com/r/piraterogue/