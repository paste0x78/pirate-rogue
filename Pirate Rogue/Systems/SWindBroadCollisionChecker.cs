﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using Artemis;
using Artemis.Manager;
using Artemis.System;
using NLog;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using Pirate_Rogue.Templates;
using RLNET;
namespace Pirate_Rogue.Systems
{
    public class SWindBroadCollisionChecker : EntitySystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        private static Logger logger;

        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            if(logger==null)
                logger = LogManager.GetLogger(this.GetType().Name);
        }

        public SWindBroadCollisionChecker() : base(typeof(CWindCollider))
        {

        }

        protected override void ProcessEntities(IDictionary<int,Entity> entities)
        {
            int screenWidth = rootConsole.Width;
            int screenHeight = rootConsole.Height;
/*            List<Tuple<Vector2,Vector2>> windRaycastsList = new List<Tuple<Vector2, Vector2>>(screenWidth);
            List<Entity> raycastEntities = new List<Entity>(screenWidth);
            //TODO: take every entity from GroupManager instead?
            foreach (var entry in entities)
            {
                if (entry.Value.HasComponent<CWindRaycast>())
                {
                    raycastEntities.Add(entry.Value);
                    CWindRaycast raycast = entry.Value.GetComponent<CWindRaycast>();
                    windRaycastsList.Add(new Tuple<Vector2, Vector2>(raycast.StartPoint,raycast.EndPoint));
                }

            }*/
            int collisions = 0;
            CWindAndOceanSettings cWindAndOceanSettings = entityWorld.TagManager.GetEntity(Globals.EnitityTags.WindSettings.ToString()).GetComponent<CWindAndOceanSettings>();

            foreach (var entry in entities)
            {

                if (entry.Value.HasComponent<CWindCollider>())
                {
                    CWindCollider cWindCollider = entry.Value.GetComponent<CWindCollider>();
                    CTransform cTransform = entry.Value.GetComponent<CTransform>();


                    Vector2 res;

                    float shipAngle = cTransform.Angle;
                    float sailsAngle = cWindCollider.AngleOffset + shipAngle;
                    float angleSailsDiff = Math.Abs(sailsAngle - cWindAndOceanSettings.Angle);
                    float angleShipDiff = Math.Abs(shipAngle - cWindAndOceanSettings.Angle);
                    if (angleSailsDiff > 180)
                        angleSailsDiff = 180 - angleSailsDiff%180;

                    //logger.Debug(angleSailsDiff);

                    CVelocity velocity = entry.Value.GetComponent<CVelocity>();

                    float maxvelocity = cWindAndOceanSettings.Knots* cWindCollider.SailsLength/2f*(1f-(angleSailsDiff/(angleSailsDiff<90f?100f:angleSailsDiff)));

                    velocity.Velocity += Globals.MathHelpers.AngularVectorForGivenDegrees(shipAngle)* maxvelocity*(float)rootConsole.DeltaInSeconds;
                    if (velocity.Velocity.X > maxvelocity)
                        velocity.Velocity.X = maxvelocity;
                    else if (velocity.Velocity.X < -maxvelocity)
                        velocity.Velocity.X = -maxvelocity;
                    if (velocity.Velocity.Y > maxvelocity)
                        velocity.Velocity.Y = maxvelocity;
                    else if (velocity.Velocity.Y < -maxvelocity)
                        velocity.Velocity.Y = -maxvelocity;
                }
            }
            #region old methods
            /*
                                Vector2 topLeftPos = entry.Value.GetComponent<CTransform>().Position +
                                         cWindCollider.PositionOffset + cWindCollider.collisionRotatedModelTopLeft;
                    Vector2 bottomLeftPos = entry.Value.GetComponent<CTransform>().Position +
                                            cWindCollider.PositionOffset + cWindCollider.collisionRotatedModelBottomLeft;
                    Vector2 topRightPos = entry.Value.GetComponent<CTransform>().Position +
                                          cWindCollider.PositionOffset + cWindCollider.collisionRotatedModelTopRight;
                    Vector2 bottomRightPos = entry.Value.GetComponent<CTransform>().Position +
                                             cWindCollider.PositionOffset +
                                             cWindCollider.collisionRotatedModelBottomRight;                 
            CVelocity velocity = entry.Value.GetComponent<CVelocity>();
                                        for (int i = windRaycastsList.Count - 1; i > -1; i--)
                                        {
                                            if (remainingCollisionWidth <= 0)
                                                break;
                                            //TODO: instead doing 2 step checking, generate real collision rectangle (2nd step) at start of the 
                                            //game and just rotate it whenever angle changes (utilized cTransform.AngleChanged) and do checking on real rectangle only
                                            Vector2 windPos1 = windRaycastsList[i].Item1;
                                            Vector2 windPos2 = windRaycastsList[i].Item2;
                                            bool intersects = true;
                    /*                        if (Intersects(windPos1, windPos2, topLeftPos, bottomLeftPos, out res))
                                            {
                                                angleBetweenRadians= Math.Acos(Vector2.Dot((windPos2 - windPos1).Normalized(),(bottomLeftPos - topLeftPos).Normalized()));
                                            }#1#
                                            if (Intersects(windPos1, windPos2, bottomLeftPos, bottomRightPos, out res))
                                            {
                                                angleBetweenRadians = Math.Acos(Vector2.Dot((windPos2 - windPos1).Normalized(), (bottomRightPos - bottomLeftPos).Normalized()));
                                                Entity e = entityWorld.CreateEntity();
                                                CTransform cTr = e.AddComponentFromPool<CTransform>();
                                                cTr.Position = res;
                                                e.AddComponent<CWindCollisionModel>(new CWindCollisionModel());
                                            }
                    /*                        else if (Intersects(windPos1, windPos2, bottomRightPos, topRightPos, out res))
                                            {
                                                angleBetweenRadians = Math.Acos(Vector2.Dot((windPos2 - windPos1).Normalized(), (bottomRightPos - topRightPos).Normalized()));
                                            }
                                            else if (Intersects(windPos1, windPos2, topRightPos, topLeftPos, out res))
                                            {
                                                angleBetweenRadians = Math.Acos(Vector2.Dot((windPos2 - windPos1).Normalized(), (topRightPos - topLeftPos).Normalized()));
                                            }#1#
                                            else
                                            {
                                                intersects = false;
                                            }
                                            if (intersects)
                                            {
                                                //calculate how much of collsion area there is perpendicularly to wind angle
                                                //TODO: take wind angle to account
                                                //TODO: take account for each raycast instead using one and discarding search?

                                                RLDynamicObject model = cWindCollider.WholeModel;
                                                RexReaderHelper.RotateModel(ref model, angle%360);

                                                //TODO: instead cTransform.Angle, calculate angle between wind and windcollider angle

                                                angleBetweenDegrees = angleBetweenRadians*Globals.Values.DegreesInRadian;
                                                velocity.Velocity += Globals.Helpers.AngularVectorForGivenRadians(angleBetweenRadians);//*10f*(float) rootConsole.DeltaInSeconds;
                                                //TODO: make wind velocity and another system that will add this to general velocity

                                                windRaycastsList.RemoveAt(i);
                                                collisions++;
                                                remainingCollisionWidth--;
                                                //remainingCollisionWidth = 0;
                                                continue;

                                                #region old method

                                                /*                            RLDynamicObject model = cWindCollider.WholeModel;
                                                        RexReaderHelper.RotateModel(ref model, angle);
                                                        bool foundForRayCast = false;
                                                        float cellsWidth = model.rotatedCells.GetLength(0);
                                                        float cellsHeight = model.rotatedCells.GetLength(1);
                                                        float halfCellsWidth = cellsWidth/2f;
                                                        float halfCellsHeight = cellsHeight/2f;#1#
                                                /*                            for (int x = 0; x < cellsWidth; x++)
                                                                                {
                                                                                    for (int y = 0; y < cellsHeight; y++)
                                                                                    {

                                                                                        if (model.rotatedCells[x, y].character > 1)
                                                                                        {

                                                                                            Vector2 xy = new Vector2(x + 1f, y + 1f);
                                                                                            //Globals.Helpers.RotatePointAroundCenter(ref xy, halfCellsWidth, halfCellsHeight, angle);
                                                                                            int iX = (int) (topLeftPos.X + xy.X);
                                                                                            int iY = (int) (topLeftPos.Y + xy.Y);

                                                                                            float resz = ((windPos2.X - windPos1.X)*(iY - windPos1.Y)) -
                                                                                                         ((iX - windPos1.X)*(windPos2.Y - windPos1.Y));
                                                                                            if (resz > 1f || resz < -1f)
                                                                                                continue;
                                                                                            if (i >= windRaycastsList.Count)
                                                                                                continue;
                                                                                            if (usedCells.ContainsKey(x))
                                                                                            {
                                                                                                if (usedCells[x].Contains(y))
                                                                                                {
                                                                                                    windRaycastsList.RemoveAt(i);
                                                                                                    //collisions++;
                                                                                                    //foundForRayCast = true;
                                                                                                    continue;
                                                                                                }
                                                                                            }
                                                                                            CVelocity velocity = entry.Value.GetComponent<CVelocity>();
                                                                                            //TODO: instead cTransform.Angle, calculate angle between wind and windcollider angle
                                                                                            velocity.Velocity +=
                                                                                                Globals.Helpers.AngularVectorForGivenDegrees(angle) *10f*
                                                                                                (float) rootConsole.DeltaInSeconds;
                                                                                            //TODO: make wind velocity and another system that will add this to general velocity
                                                                                            if (velocity.Velocity.X > 4f)
                                                                                                velocity.Velocity.X = 4f;
                                                                                            else if (velocity.Velocity.X < -4f)
                                                                                                velocity.Velocity.X = -4f;
                                                                                            if (velocity.Velocity.Y > 4f)
                                                                                                velocity.Velocity.Y = 4f;
                                                                                            else if (velocity.Velocity.Y < -4f)
                                                                                                velocity.Velocity.Y = -4f;

                                                                                            windRaycastsList.RemoveAt(i);
                                                                                            collisions++;
                                                                                            foundForRayCast = true;
                                                                                            if (!usedCells.ContainsKey(x))
                                                                                            {
                                                                                                usedCells.Add(x, new List<int> {y});
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                usedCells[x].Add(y);
                                                                                            }
                                                                                            break;

                                                                                        }
                                                                                    }
                                                                                    if (foundForRayCast)
                                                                                        break;
                                                                                }
                                                                            #1#

                                                #endregion
                                            }

                                        }

                                        float maxvelocity = 4f * maxCollisionWidth / 8f;
                                        if (velocity.Velocity.X > maxvelocity)
                                            velocity.Velocity.X = maxvelocity;
                                        else if (velocity.Velocity.X < -maxvelocity)
                                            velocity.Velocity.X = -maxvelocity;
                                        if (velocity.Velocity.Y > maxvelocity)
                                            velocity.Velocity.Y = maxvelocity;
                                        else if (velocity.Velocity.Y < -maxvelocity)
                                            velocity.Velocity.Y = -maxvelocity;
                                        if (collisions > 0)
                                        {
                                            logger.Debug("Collisions: " + collisions+" (max: "+maxCollisionWidth+")\nvel:"+ velocity.Velocity);
                                        }
                                    }

                                }
                                foreach (Entity entity in raycastEntities)
                                {
                                    if (entity == null || entity.DeletingState)
                                        continue;
                                    entity.GetComponent<CWindRaycast>().CleanUp();
                                    entity.Delete();
                                }
                                */
            #endregion
            base.ProcessEntities(entities);

        }
        bool Intersects(Vector2 a1, Vector2 a2, Vector2 b1, Vector2 b2, out Vector2 intersection)
        {
            intersection = Vector2.Zero;

            Vector2 b = a2 - a1;
            Vector2 d = b2 - b1;
            float bDotDPerp = b.X * d.Y - b.Y * d.X;

            // if b dot d == 0, it means the lines are parallel so have infinite intersection points
            if (bDotDPerp == 0)
                return false;

            Vector2 c = b1 - a1;
            float t = (c.X * d.Y - c.Y * d.X) / bDotDPerp;
            if (t < 0 || t > 1)
                return false;

            float u = (c.X * b.Y - c.Y * b.X) / bDotDPerp;
            if (u < 0 || u > 1)
                return false;

            intersection = a1 + t * b;

            return true;
        }
    }
}
