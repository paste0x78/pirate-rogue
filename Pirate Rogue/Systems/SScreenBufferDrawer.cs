﻿using Artemis.System;
using NLog;
using Pirate_Rogue.Singletones;
using RLNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pirate_Rogue.Systems
{
    class SScreenBufferDrawer : ProcessingSystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            if (logger == null)
                logger = LogManager.GetLogger(this.GetType().Name);
        }

        public override void ProcessSystem()
        {
            rootConsole.DrawGameScreenBuffer(Globals.ReadOnlyValues.TransparentColor);
        }
    }
}
