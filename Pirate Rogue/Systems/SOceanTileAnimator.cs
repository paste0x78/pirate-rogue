﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using OpenTK;
using Pirate_Rogue.Globals;
using RLNET;

namespace Pirate_Rogue.Systems
{
    class SOceanTileAnimator : EntityComponentProcessingSystem<CAnimatedTile, CTransform>
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
        }



        public override void Process(Entity entity, CAnimatedTile cTile, CTransform cTransform)
        {
            //TODO: use shaders this way: generate whole buffer once, then based on wind direction move buffer and regenerate only empty/collision tiles
            //TODO: move this code to rlrootconsole, because DRY
            
            Vector2 startPosition = cTransform.Position+ camera.position+new Vector2(0,cTile.DeltaPosition);

            /*            for (float x = startPosition.X; x < camera.position.X + rootConsole.Width + cTile.Width+5; x += cTile.Width)
                        {
                            for (float y = startPosition.Y; y < camera.position.Y + rootConsole.Height + cTile.Height; 
                                y += cTile.Height*(cTile.Layer!=4?1:(1f+cTile.Layer/(float)random.Next(8,9))*(cTile.Layer/1.2f)))
                            {
                                for(int xi=0;xi<cTile.Width;xi++)
                                {
                                    for(int yi=0;yi<cTile.Height;yi++)
                                    {
                                        RLCell cell = cTile.Models[cTile.CurrentFrame].cells[xi, yi];
                                        if (cell.character < 0 || cell.color == Globals.Values.TransparentColor)
                                            continue;
                                        rootConsole.SetGameScreenBuffer(x + xi, y + yi, cell);

                                    }
                                }
                                //rootConsole.DrawDynamicObject(cTile.Model, new Vector2(x,y), cTransform.Angle, camera.position);

                            }               
                        }*/
            for (int xi = 0; xi < cTile.Width; xi++)
            {
                for (int yi = 0; yi < cTile.Height; yi++)
                {
                    RLCell cell = cTile.Models[cTile.CurrentFrame].renderedCells[xi, yi];
                    if (cell.character < 0 || cell.color == Globals.ReadOnlyValues.TransparentColor)
                        continue;
                    rootConsole.SetGameScreenBuffer(startPosition.X + xi, startPosition.Y + yi, cell);

                }
            }

            cTile.DeltaPosition += (float)rootConsole.DeltaInSeconds*2f*(4f-cTile.Layer)/4f;
            
            if (cTile.FrameTimer >= cTile.SecondsBetweenFrames)
            {
                cTile.FrameTimer = 0;
                cTile.CurrentFrame++;
                if (cTile.CurrentFrame >= cTile.Models.Count)
                    cTile.CurrentFrame = 0;
                return;
            }
            cTile.FrameTimer += (float)(rootConsole.DeltaInSeconds+RandomHelpers.Random.NextDouble())/5f;
        }
    }
}
