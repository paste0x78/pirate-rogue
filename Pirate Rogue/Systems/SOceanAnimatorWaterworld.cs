﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using System.Collections.Generic;
using NLog;
using OpenTK;
using RLNET;

namespace Pirate_Rogue.Systems
{
    class SOceanAnimatorWaterworld : EntityProcessingSystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        private static ILogger logger = LogManager.GetCurrentClassLogger();

        float coast2water_fadedepth = 0.10f;
        float large_waveheight = 0.50f; // change to adjust the "heavy" waves
        float large_wavesize = 4.0f;  // factor to adjust the large wave size
        float small_waveheight = 0.6f;  // change to adjust the small random waves
        float small_wavesize = 0.5f;   // factor to ajust the small wave size
        float water_softlight_fact = 15.0f;  // range [1..200] (should be << smaller than glossy-fact)
        float water_glossylight_fact = 120.0f; // range [1..200]
        float particle_amount = 70.0f;
        Vector3 watercolor = new Vector3(0.43f, 0.60f, 0.66f); // 'transparent' low-water color (RGB)
        Vector3 watercolor2 = new Vector3(0.06f, 0.07f, 0.11f); // deep-water color (RGB, should be darker than the low-water color)
        Vector3 water_specularcolor = new Vector3(1.3f, 1.3f, 0.9f);    // specular Color (RGB) of the water-highlights
        private Vector3 light;
        private static Random random;

        private Vector2 iResolution;

        Matrix3 fromEuler(Vector3 ang)
        {
            Vector2 a1 = new Vector2((float) Math.Sin(ang.X), (float) Math.Cos(ang.X));
            Vector2 a2 = new Vector2((float) Math.Sin(ang.Z), (float) Math.Cos(ang.Y));
            Vector2 a3 = new Vector2((float) Math.Sin(ang.Y), (float) Math.Cos(ang.Z));
            Matrix3 m = new Matrix3(
            new Vector3(a1.Y * a3.Y + a1.X * a2.X * a3.X, a1.Y * a2.X * a3.X + a3.Y * a1.X, -a2.Y * a3.X),
            new Vector3(-a2.Y * a1.X, a1.Y * a2.Y, a2.X),
            new Vector3(a3.Y * a1.X * a2.X + a1.Y * a3.X, a1.X * a3.X - a1.Y * a3.Y * a2.X, a2.Y * a3.Y));
            return m;
        }

        float fract(float val)
        {
            return (float) (val - Math.Floor(val));
        }
        Vector2 fract(Vector2 val)
        {
            return new Vector2(fract(val.X),fract(val.Y));
        }
        float hash(Vector2 p)
        {
            float h = Vector2.Dot(p, new Vector2(127.1f, 311.7f));
            return fract((float) (Math.Sin(h) * 43758.5453123));
        }

        Vector3 reflect(Vector3 N, Vector3 I)
        {
            return I - 2.0f*Vector3.Dot(N, I)*N;
        }
        float mix(float v1, float v2, float a)
        {

            return v1 * (1 - a) + v2 * a;
        }
        Vector2 mix(Vector2 v1, Vector2 v2, Vector2 a)
        {

            return v1 * (new Vector2(1f-a.X,1f-a.Y)) + v2 * a;
        }
        Vector3 mix(Vector3 v1, Vector3 v2, float a)
        {

            return v1 * (new Vector3(1f - a, 1f - a,1f-a)) + v2 * a;
        }
        private float noise(Vector3 uvt)
        {
            Vector2 p = new Vector2(uvt.X,uvt.Y);
            Vector2 ft = fract(uvt.Z*new Vector2(1.0f, 1.0f));
            Vector2 i = new Vector2((float) Math.Floor(p.X+ft.X+Math.Floor(uvt.Z)),(float) Math.Floor(p.Y+ft.Y+Math.Floor(uvt.Z)));
            Vector2 f = fract(p + ft);
            Vector2 u = f*f*(new Vector2(3.0f,3.0f) - 2.0f*f);
            return (float) (-1.0 + 2.0*mix((float) mix(hash(i + new Vector2(0.0f, 0.0f)),
                hash(i + new Vector2(1.0f, 0.0f)), u.X),
                (float) mix(hash(i + new Vector2(0.0f, 1.0f)),
                    hash(i + new Vector2(1.0f, 1.0f)), u.X), u.Y));
        }

        Vector2 mult(Vector2 v, Matrix2 m)
        {
            return new Vector2(
                m.M11 * v.X + m.M12 * v.Y,
                m.M21 * v.X + m.M22 * v.Y);
        }
        Vector3 mult(Vector3 v, Matrix3  m)
        {

            return new Vector3(
                m.M11 * v.X + m.M12 * v.Y + m.M13 * v.Z,
                m.M21 * v.X + m.M22 * v.Y + m.M23 * v.Z,
                m.M31 * v.X + m.M32 * v.Y + m.M33 * v.Z);
        }

        float smoothstep(float edge0, float edge1, float x)
        {
            float t = (float) MathHelper.Clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
            return t * t * (3.0f - 2.0f * t);
        }

        // calculate random value
        float hash(float n)
        {
            return fract((float) (Math.Sin(n) * 43758.5453123));
        }
        // 2d noise function
        float noise1(Vector2 x)
        {
            Vector2 p = new Vector2((float) Math.Floor(x.X),(float) Math.Floor(x.Y));
            Vector2 f = new Vector2(smoothstep(0.0f, 1.0f, fract(x.X)), smoothstep(0.0f, 1.0f, fract(x.Y)));
            float n = p.X + p.Y * 57.0f;
            return mix(mix(hash(n + 0.0f), hash(n + 1.0f), f.X),
              mix(hash(n + 57.0f), hash(n + 58.0f), f.X), f.Y);
        }
        float noise(Vector2 p)
        {
            return (float) random.NextDouble();
        }

        float height_map(Vector2 p)
        {
            int indexX = (int) (p.X*100f)+80;
            int indexY = (int)(p.Y * 100f)+80;
            if (indexX < 0 || indexX >= heightMapArray.GetLength(0) || indexY < 0 ||
                indexY >= heightMapArray.GetLength(0))
                return 0f;
            if (heightMapArray[indexX,indexY] > -100f)
                return heightMapArray[indexX,indexY];
            Matrix2 m = new Matrix2(0.9563f * 1.4f, -0.2924f * 1.4f, 0.2924f * 1.4f, 0.9563f * 1.4f);
            p *= 6.0f;
            float f = 0.6000f * noise1(p); p =mult(p,m) * 1.1f;
            f += 0.2500f * noise1(p); p = mult(p, m) * 1.32f;
            f += 0.1666f * noise1(p); p = mult(p, m) * 1.11f;
            f += 0.0834f * noise(p); p = mult(p, m) * 1.12f;
            f += 0.0634f * noise(p); p = mult(p, m) * 1.13f;
            f += 0.0444f * noise(p); p = mult(p, m) * 1.14f;
            f += 0.0274f * noise(p); p = mult(p, m) * 1.15f;
            f += 0.0134f * noise(p); p = mult(p, m) * 1.16f;
            f += 0.0104f * noise(p); p = mult(p, m) * 1.17f;
            f += 0.0084f * noise(p);
            float FLAT_LEVEL = 0.525f;
            if (f < FLAT_LEVEL)
                f = f;
            else
                f = (float) (Math.Pow((f - FLAT_LEVEL) / (1.0f- FLAT_LEVEL), 2.0f) * (1.0f- FLAT_LEVEL) * 2.0f + FLAT_LEVEL); // makes a smooth coast-increase
            heightMapArray[indexX, indexY] = MathHelper.Clamp(f, 0.0f, 10.0f);
            return heightMapArray[indexX,indexY];
        }
        Vector3 terrain_map(Vector2 p) //color
        {
            return new Vector3(0.7f, 0.55f, 0.4f); // test-terrain is simply 'sandstone'
        }

        Matrix2 m = new Matrix2(0.72f, -1.60f, 1.60f, 0.72f);


        float water_map(Vector2 p, float height)
        {
            Vector2 p2 = p * large_wavesize;
            Vector2 shift1 = 0.001f * new Vector2((float) (rootConsole.TotalTime * 160.0f), (float) (rootConsole.TotalTime * 120.0f));
            Vector2 shift2 = 0.001f * new Vector2((float) (rootConsole.TotalTime * 190.0f), (float) (-rootConsole.TotalTime * 130.0f));

            // coarse crossing 'ocean' waves...
            float f = 0.6000f * noise(p);
            f += 0.2500f * noise(mult(p,m));
            f += 0.1666f * noise(mult(mult(p, m), m));
            float wave = (float) (Math.Sin(p2.X * 0.622 + p2.Y * 0.622 + shift2.X * 4.269) * large_waveheight * f * height * height);

            p *= small_wavesize;
            f = 0.0f;
            float amp = 1.0f;
            float s =0.5f;
            for (int i = 0; i < 3; i++)
            {
                p = mult(p, m)*0.947f;
                f -= amp*(float)Math.Abs(Math.Sin((noise(p + shift1*s) - 0.5f)*2.0f));
                amp = amp*0.59f;
                s *= -1.329f;
            }

            return wave + f * small_waveheight;
        }
        float nautic(Vector2 p)
        {
            p *= 18.0f;
            float f = 0.0f;
            float amp = 1.0f;
            float s = 0.5f;
            for (int i = 0; i < 2; i++)
            {
                p = mult(p, m) * 1.2f;
                f += amp* (float)Math.Abs((smoothstep(0.0f, 1.0f, noise(new Vector2((float) (p.X+rootConsole.TotalTime*s),(float) (p.Y+rootConsole.TotalTime*s)))) - 0.5f));
                amp = amp*0.5f;
                s *= -1.227f;
            }
            return (float) Math.Pow(1.0f- f, 5.0f);
        }
        float particles(Vector2 p)
        {
            p *= 200.0f;
            float f = 0.0f;
            float amp = 1.0f;
            float s = 1.5f;
            for (int i = 0; i < 2; i++)
            {
                p = mult(p,m)*1.2f;
                f += amp*noise(new Vector2((float)(p.X + rootConsole.TotalTime * s), (float)(p.Y + rootConsole.TotalTime * s)));
                amp = amp*0.5f;
                s *= -1.227f;
            }
            return (float) (Math.Pow(f * 0.35f, 7.0f) * particle_amount);
        }
        float test_shadow(Vector2 xy, float height)
        {
            Vector3 r0 = new Vector3(xy.X,xy.Y, height);
            Vector3 rd = Vector3.Normalize(light - r0);

            float hit = 1.0f;
            float t = 0.001f;
            for (int j = 1; j < 4; j++)
            {
                Vector3 p = r0 + t * rd;
                float h = height_map(new Vector2(p.X,p.Y));
                float height_diff = p.Z - h;
                if (height_diff < 0.0)
                {
                    return 0.0f;
                }
                t += 0.01f + height_diff * 0.02f;
                hit = Math.Min(hit, 2.0f* height_diff / t); // soft shaddow   
            }
            return hit;
        }
        Vector3 CalcTerrain(Vector2 uv, float height)
        {
            Vector3 col = terrain_map(uv);
            float h1 = height_map(uv - new Vector2(0.0f, 0.01f));
            float h2 = height_map(uv + new Vector2(0.0f, 0.01f));
            float h3 = height_map(uv - new Vector2(0.01f, 0.0f));
            float h4 = height_map(uv + new Vector2(0.01f, 0.0f));
            Vector3 norm = Vector3.Normalize(new Vector3(h3 - h4, h1 - h2, 1.0f));
            Vector3 r0 = new Vector3(uv.X,uv.Y, height);
            Vector3 rd = Vector3.Normalize(light - r0);
            float grad = Vector3.Dot(norm, rd);
            col *= grad + (float) Math.Pow(grad, 8.0f);
            float terrainshade = test_shadow(uv, height);
            col = mix(col * 0.25f, col, terrainshade);
            return col;
        }

        private float[,] heightMapArray;
        protected override void Begin()
        {
           
            base.Begin();
            if(random==null)
                random = new Random();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            iResolution = new Vector2(rootConsole.Width, rootConsole.Height) * multiplier;
            //TODO: move indexers to const
            heightMapArray = new float[200,200];
            for (int x = 0; x < heightMapArray.GetLength(0); x++)
            {
                for (int y = 0; y < heightMapArray.GetLength(1); y++)
                {
                    heightMapArray[x,y] = -100f;
                }
            }
        }


        public override void Process(Entity entity)
        {
            if (entity.HasComponent<CTile>()&&entity.HasComponent<CTransform>())
                Process(entity,entity.GetComponent<CTile>(),entity.GetComponent<CTransform>());
        }

        private float multiplier = 1f;
        protected override void ProcessEntities(IDictionary<int, Entity> entities)
        {
            foreach (RLKeyEvent key in rootConsole.Keyboard.PollKeyEvents())
            {
                switch (key.Char)
                {
                    case 'y':
                        multiplier += 0.1f;
                        break;
                    case 'h':
                        multiplier -= 0.1f;
                        break;
                }
            }
            rootConsole.Print(0, rootConsole.Height - 1,"multi:"+multiplier, RLColor.Gray, RLColor.Blue);
            light = new Vector3(-0f, (float)(Math.Sin(rootConsole.TotalTime * 0.5) * .5 + .35), 2.8f); // position of the sun
            iResolution = new Vector2(rootConsole.Width, rootConsole.Height) * multiplier;
            base.ProcessEntities(entities);
        }


        void Process(Entity entity, CTile cTile, CTransform cTransform)
        {
            Vector2 cameraEnd = camera.position + new Vector2(rootConsole.Width, rootConsole.Height);
            Vector3 fragColor = new Vector3();



            Vector2 startPosition = cTransform.Position;
            if (cTransform.Position.X<0||cTransform.Position.Y<0)
                return;
            int nX = 0, nY = 0;
            for (int ix = (int) startPosition.X; ix < startPosition.X + cTile.Width; ix++)
            {
                nX++;
                for (int iy = (int) startPosition.Y; iy < startPosition.Y + cTile.Height; iy++)
                {
                    nY++;
                    Vector2 fragCoord = new Vector2(ix, iy);
                    //if (fragCoord.X < 0 || fragCoord.X > 1f || fragCoord.Y < 0 || fragCoord.Y > 1f)
                    //    continue;
                    Vector2 uv = new Vector2(fragCoord.X/rootConsole.Width, fragCoord.Y/rootConsole.Height) - 
                                 //new Vector2(cTile.OffsetX/ (startPosition.X + cTile.Width), cTile.OffsetY / (startPosition.Y + cTile.Height))-
                                 new Vector2(-0.12f, 0.25f);
                    float WATER_LEVEL = 2f; // Water level (range: 0.0 - 2.0)

                    if ((nX == 1 && nY == 1) || (nX == cTile.Width-2 && nY == cTile.Height-2))
                    {
                        //logger.Debug(uv);
                    }
                    float deepwater_fadedepth = 0.5f + coast2water_fadedepth;

                    float height = height_map(uv);
                    Vector3 col = new Vector3();

                    float waveheight = MathHelper.Clamp(WATER_LEVEL*3.0f - 1.5f, 0.0f, 1.0f);
                    float level = WATER_LEVEL +
                                  0.2f*
                                  water_map(uv*15.0f + new Vector2((float) rootConsole.TotalTime, 0.1f), waveheight);
                    if (height > level)
                    {
                        col = CalcTerrain(uv, height);
                    }
                    if (height <= level)
                    {
                        Vector2 dif = new Vector2(0.0f, 0.01f);
                        Vector2 pos = uv*15.0f + new Vector2((float) (rootConsole.TotalTime*0.01));
                        float h1 = water_map(pos - dif, waveheight);
                        float h2 = water_map(pos + dif, waveheight);
                        float h3 = water_map(pos - new Vector2(dif.Y, dif.X), waveheight);
                        float h4 = water_map(pos + new Vector2(dif.Y, dif.X), waveheight);
                        Vector3 normwater = Vector3.Normalize(new Vector3(h3 - h4, h1 - h2, 0.125f));
                            // norm-vector of the 'bumpy' water-plane
                        uv += new Vector2(normwater.X, normwater.Y)*0.002f*(level - height);

                        col = CalcTerrain(uv, height);

                        float coastfade = MathHelper.Clamp((level - height)/coast2water_fadedepth, 0.0f, 1.0f);
                        float coastfade2 = MathHelper.Clamp((level - height)/deepwater_fadedepth, 0.0f, 1.0f);
                        float intensity = col.X*0.2126f + col.Y*0.7152f + col.Z*0.0722f;
                        watercolor = mix(watercolor*intensity, watercolor2, smoothstep(0.0f, 1.0f, coastfade2));

                        Vector3 r0 = new Vector3(uv.X, uv.Y, WATER_LEVEL);
                        Vector3 rd = Vector3.Normalize(light - r0); // ray-direction to the light from water-position
                        float grad = Vector3.Dot(normwater, rd); // dot-product of norm-vector and light-direction
                        float specular = (float) Math.Pow(grad, water_softlight_fact);
                            // used for soft highlights                          
                        float specular2 = (float) Math.Pow(grad, water_glossylight_fact); // used for glossy highlights
                        float gradpos = Vector3.Dot(new Vector3(0.0f, 0.0f, 1.0f), rd);
                        float specular1 = smoothstep(0.0f, 1.0f, (float) Math.Pow(gradpos, 5.0f));
                            // used for diffusity (some darker corona around light's specular reflections...)                          
                        float watershade = test_shadow(uv, level);
                        watercolor *= 2.2f + watershade;
                        float temp = (0.2f + 0.8f*watershade)*((grad - 1.0f)*0.5f + specular)*0.25f;
                        watercolor += new Vector3(temp, temp, temp);
                        watercolor /= (1.0f + specular1*1.25f);
                        watercolor += watershade*specular2*water_specularcolor;
                        watercolor += watershade*coastfade*(1.0f - coastfade2)*
                                      (new Vector3(0.5f, 0.6f, 0.7f)*nautic(uv)+new Vector3(1.0f, 1.0f, 1.0f)*particles(uv));

                        col = mix(col, watercolor, coastfade);
                        int localX = ix%cTile.Width;
                        int localY = iy%cTile.Height;
                        if (localX < 0)
                            localX += cTile.Width;
                        if (localY < 0)
                            localY += cTile.Height;
                        RLColor colorNew = new RLColor(fragColor.X, fragColor.Y, fragColor.Z);
                        cTile.Model.renderedCells[localX, localY].backColor = colorNew;
                        cTile.Model.renderedCells[localX, localY].color = colorNew;
                        cTile.Model.FrontColorChanged = true;
                    }

                    fragColor = col;
                }
            }
            // rootConsole.DrawDynamicObject(cTile.Model, new Vector2(x,y), cTransform.Angle, camera.position, cTile.Model.ModelChanged);

        }

        public SOceanAnimatorWaterworld(Type requiredType, params Type[] otherTypes) : base(requiredType, otherTypes)
        {
        }

        public SOceanAnimatorWaterworld() : base(Aspect.All(typeof(CTile),typeof(CTransform)))
        {
        }
    }
}
