﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using RLNET;

namespace Pirate_Rogue.Systems
{
    public class SFPSCalculator : EntityComponentProcessingSystem<CFpsCounter>
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
        }

        public override void Process(Entity entity, CFpsCounter fpsCounter)
        {
            fpsCounter.Time += rootConsole.DeltaInSeconds;

            if (fpsCounter.Time < 1.0)
            {
                fpsCounter.Frames++;
            }
            else
            {
                fpsCounter.FPS = (int)fpsCounter.Frames;
                fpsCounter.Time = 0.0;
                fpsCounter.Frames = 0.0;
            }
        }
    }
}
