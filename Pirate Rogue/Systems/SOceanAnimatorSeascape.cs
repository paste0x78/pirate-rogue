﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using System.Collections.Generic;
using OpenTK;
using RLNET;

namespace Pirate_Rogue.Systems
{
    class SOceanAnimatorSeascape : ParallelEntityProcessingSystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;

        const int NUM_STEPS = 8;
        const double PI = 3.1415;
        const double EPSILON = 1e-3;
        private double EPSILON_NRM;
        const int ITER_GEOMETRY = 1;
        const int ITER_FRAGMENT = 2;
        const double SEA_HEIGHT = 0.6;
        const double SEA_CHOPPY = 4.0;
        const double SEA_SPEED = 0.3;
        const double SEA_FREQ = 0.16;
        Vector3 SEA_BASE = new Vector3(0.1f, 0.19f, 0.22f);
        Vector3 SEA_WATER_COLOR = new Vector3(0.8f, 0.9f, 0.6f);
        private float SEA_TIME;
        Matrix2 octave_m = new Matrix2(1.6f, 1.2f, -1.2f, 1.6f); 
        private Vector2 iResolution;

        Matrix3 fromEuler(Vector3 ang)
        {
            Vector2 a1 = new Vector2((float) Math.Sin(ang.X), (float) Math.Cos(ang.X));
            Vector2 a2 = new Vector2((float) Math.Sin(ang.Z), (float) Math.Cos(ang.Y));
            Vector2 a3 = new Vector2((float) Math.Sin(ang.Y), (float) Math.Cos(ang.Z));
            Matrix3 m = new Matrix3(
            new Vector3(a1.Y * a3.Y + a1.X * a2.X * a3.X, a1.Y * a2.X * a3.X + a3.Y * a1.X, -a2.Y * a3.X),
            new Vector3(-a2.Y * a1.X, a1.Y * a2.Y, a2.X),
            new Vector3(a3.Y * a1.X * a2.X + a1.Y * a3.X, a1.X * a3.X - a1.Y * a3.Y * a2.X, a2.Y * a3.Y));
            return m;
        }

        float fract(float val)
        {
            return (float) (val - Math.Floor(val));
        }
        Vector2 fract(Vector2 val)
        {
            return new Vector2(fract(val.X),fract(val.Y));
        }
        float hash(Vector2 p)
        {
            float h = Vector2.Dot(p, new Vector2(127.1f, 311.7f));
            return fract((float) (Math.Sin(h) * 43758.5453123));
        }

        Vector3 reflect(Vector3 N, Vector3 I)
        {
            return I - 2.0f*Vector3.Dot(N, I)*N;
        }
        float mix(float v1, float v2, float a)
        {

            return v1 * (1 - a) + v2 * a;
        }
        Vector2 mix(Vector2 v1, Vector2 v2, Vector2 a)
        {

            return v1 * (new Vector2(1f-a.X,1f-a.Y)) + v2 * a;
        }
        Vector3 mix(Vector3 v1, Vector3 v2, float a)
        {

            return v1 * (new Vector3(1f - a, 1f - a,1f-a)) + v2 * a;
        }
        private float noise(Vector3 uvt)
        {
            Vector2 p = new Vector2(uvt.X,uvt.Y);
            Vector2 ft = fract(uvt.Z*new Vector2(1.0f, 1.0f));
            Vector2 i = new Vector2((float) Math.Floor(p.X+ft.X+Math.Floor(uvt.Z)),(float) Math.Floor(p.Y+ft.Y+Math.Floor(uvt.Z)));
            Vector2 f = fract(p + ft);
            Vector2 u = f*f*(new Vector2(3.0f,3.0f) - 2.0f*f);
            return (float) (-1.0 + 2.0*mix((float) mix(hash(i + new Vector2(0.0f, 0.0f)),
                hash(i + new Vector2(1.0f, 0.0f)), u.X),
                (float) mix(hash(i + new Vector2(0.0f, 1.0f)),
                    hash(i + new Vector2(1.0f, 1.0f)), u.X), u.Y));
        }
        float diffuse(Vector3 n, Vector3 l, float p)
        {
            return (float) Math.Pow(Vector3.Dot(n, l) * 0.4 + 0.6, p);
        }
        float specular(Vector3 n, Vector3 l, Vector3 e, float s)
        {
            float nrm = (float) ((s + 8.0) / (3.1415 * 8.0));
            return (float) (Math.Pow(Math.Max(Vector3.Dot(reflect(e, n), l), 0.0), s) * nrm);
        }
        Vector3 getSkyColor(Vector3 e)
        {
            e.Y = Math.Max(e.Y, 0.0f);
            Vector3 ret = new Vector3((float) Math.Pow(1.0f - e.Y, 2.0f),1.0f - e.Y,0.6f + (1.0f - e.Y) * 0.4f);
            return ret;
        }
        float sea_octave(Vector3 uvt, float choppy)
        {
            float z = noise(uvt);
            Vector2 uv = new Vector2(uvt.X+z,uvt.Y+z);// + uvt.z;
            float cosT = (float) Math.Cos(uvt.Z);
            float sinT = (float) Math.Sin(uvt.Z);
            Vector2 cost = new Vector2(cosT, cosT);
            Vector2 sint = new Vector2(sinT, sinT);
            Vector2 sinUV = new Vector2((float) Math.Sin(uv.X),(float) Math.Sin(uv.Y));
            Vector2 cosUV = new Vector2((float)Math.Sin(uv.X), (float)Math.Sin(uv.Y));
            Vector2 res = sinUV*cost + sint*cosUV;
            Vector2 wv = new Vector2(1.0f-Math.Abs(res.X),1.0f-Math.Abs(res.Y));
            Vector2 res2 = cosUV * cost - sinUV * sint;
            Vector2 swv = new Vector2(Math.Abs(res2.X),Math.Abs(res2.Y));

            wv = mix(wv, swv, wv);
            return (float) Math.Pow(1.0 - Math.Pow(wv.X * wv.Y, 0.65), choppy);
        }
        Vector2 mult(Vector2 v, Matrix2 m)
        {
            return new Vector2(
                m.M11 * v.X + m.M12 * v.Y,
                m.M21 * v.X + m.M22 * v.Y);
        }
        Vector3 mult(Vector3 v, Matrix3  m)
        {

            return new Vector3(
                m.M11 * v.X + m.M12 * v.Y + m.M13 * v.Z,
                m.M21 * v.X + m.M22 * v.Y + m.M23 * v.Z,
                m.M31 * v.X + m.M32 * v.Y + m.M33 * v.Z);
        }
        float map(Vector3 p)
        {
            double freq = SEA_FREQ;
            double amp = SEA_HEIGHT;
            float choppy = (float) SEA_CHOPPY;
            Vector3 uvt = new Vector3(p.X,p.Y, SEA_TIME); 
            uvt.X *= 0.75f;

            float d,h=0f;
            for (int i = 0; i < ITER_GEOMETRY; i++)
            {
                d = sea_octave((uvt) * (float) freq, choppy);
                d += sea_octave((uvt) * (float) freq, choppy);
                h += (float)(d * amp);
                Vector2 uv2 = new Vector2(uvt.X,uvt.Y);
                uv2 = mult(uv2, octave_m);
                uvt.X = uv2.X;
                uvt.Y = uv2.Y;
                freq *= 1.9; 
                amp *= 0.22;
                choppy = mix(choppy, 1.0f, 0.2f);
            }
            return p.Y - h;
        }
        float map_detailed(Vector3 p)
        {
            float freq = (float) SEA_FREQ;
            float amp = (float) SEA_HEIGHT;
            float choppy = (float) SEA_CHOPPY;
            Vector3 uvt = new Vector3(p.X,p.Z, SEA_TIME); 
            uvt.X *= 0.75f;

            float d; 
            float h = 0.0f;
            for (int i = 0; i < ITER_FRAGMENT; i++)
            {
                d = sea_octave((uvt) * freq, choppy);
                d += sea_octave((uvt) * freq, choppy);
                h += d * amp;
                Vector2 uv2 = new Vector2(uvt.X, uvt.Y);
                uv2 = mult(uv2, octave_m);
                uvt.X = uv2.X;
                uvt.Y = uv2.Y;
                freq *= 1.9f; 
                amp *= 0.22f;
                choppy = mix(choppy, 1.0f, 0.2f);
            }
            return p.Y - h;
        }
        Vector3 getSeaColor(Vector3 p, Vector3 n, Vector3 l, Vector3 eye, Vector3 dist)
        {
            float fresnel = 1.0f - Math.Max(Vector3.Dot(n, -eye), 0.0f);
            fresnel = (float) (Math.Pow(fresnel, 3.0f) * 0.65f);

            Vector3 reflected = getSkyColor(reflect(eye, n));
            Vector3 refracted = SEA_BASE + diffuse(n, l, 80.0f) * SEA_WATER_COLOR * 0.12f;

            Vector3 color = mix(refracted, reflected, fresnel);

            float atten = Math.Max(1.0f - Vector3.Dot(dist, dist) * 0.001f, 0.0f);
            color += SEA_WATER_COLOR * (float) (p.Y - SEA_HEIGHT) * 0.18f * atten;

            color += new Vector3(specular(n, l, eye, 60.0f));

            return color;
        }
        // tracing
        Vector3 getNormal(Vector3 p, float eps)
        {
            Vector3 n = new Vector3();
            n.Y = map_detailed(p);
            n.X = map_detailed(new Vector3(p.X + eps, p.Y, p.Z)) - n.Y;
            n.Z = map_detailed(new Vector3(p.X, p.Y, p.Z + eps)) - n.Y;
            n.Y = eps;
            return Vector3.Normalize(n);
        } 
        float heightMapTracing(Vector3 ori, Vector3 dir, ref Vector3 p)
        {
            float tm = 0.0f;
            float tx = 1000.0f;
            float hx = map(ori + dir * tx);
            if (hx > 0.0f) 
                return tx;
            float hm = map(ori + dir * tm);
            float tmid = 0.0f;
            for (int i = 0; i < NUM_STEPS; i++)
            {
                tmid = mix(tm, tx, hm / (hm - hx));
                p = ori + dir * tmid;
                float hmid = map(p);
                if (hmid < 0.0)
                {
                    tx = tmid;
                    hx = hmid;
                }
                else
                {
                    tm = tmid;
                    hm = hmid;
                }
            }
            return tmid;
        }

        float smoothstep(float edge0, float edge1, float x)
        {
            float t = (float) MathHelper.Clamp((x - edge0) / (edge1 - edge0), 0.0, 1.0);
            return t * t * (3.0f - 2.0f * t);
        }


        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            iResolution = new Vector2(rootConsole.Width, rootConsole.Height) * multiplier;
            EPSILON_NRM = 0.1 / iResolution.X; 
        }


        public override void Process(Entity entity)
        {
            if(entity.HasComponent<CTile>()&&entity.HasComponent<CTransform>())
                Process(entity,entity.GetComponent<CTile>(),entity.GetComponent<CTransform>());
        }

        private float multiplier = 0.001f;
        protected override void ProcessEntities(IDictionary<int, Entity> entities)
        {
            foreach (RLKeyEvent key in rootConsole.Keyboard.PollKeyEvents())
            {
                switch (key.Char)
                {
                    case 'y':
                        multiplier += 0.1f;
                        break;
                    case 'h':
                        multiplier -= 0.1f;
                        break;
                }
            }
            rootConsole.Print(0, rootConsole.Height - 1,"multi:"+multiplier, RLColor.Gray, RLColor.Blue);
            base.ProcessEntities(entities);
        }
        
         void Process(Entity entity, CTile cTile, CTransform cTransform)
         {
            Vector2 cameraEnd = camera.position + new Vector2(rootConsole.Width, rootConsole.Height);
            Vector3 fragColor = new Vector3();

            iResolution = new Vector2(rootConsole.Width, rootConsole.Height) * multiplier;
            EPSILON_NRM = 0.1 / iResolution.X;

            cTransform.Position = camera.position-Vector2.One;
            Vector2 startPosition = cTransform.Position;
            SEA_TIME = (float)(rootConsole.TotalTime * SEA_SPEED);
            for (int x = (int) startPosition.X; x < startPosition.X + rootConsole.Width+cTile.Width; x += cTile.Width)
            {
                for (int y = (int)startPosition.Y; y < startPosition.Y + rootConsole.Height+cTile.Height; y += cTile.Height)
                {
                    for (int ix = x; ix < x+cTile.Width; ix++)
                    {
                        for (int iy = y; iy < y + cTile.Height; iy++)
                        {
                            Vector2 fragCoord = new Vector2(ix-startPosition.X/rootConsole.Width, iy - startPosition.Y/rootConsole.Height);
                            //if (fragCoord.X < 0 || fragCoord.X > 1f || fragCoord.Y < 0 || fragCoord.Y > 1f)
                            //    continue;
                            Vector2 uv = new Vector2(fragCoord.X / iResolution.X, fragCoord.Y / iResolution.Y);
                            uv = uv * 2.0f;
                            uv.X -= 1.0f;
                            uv.Y -= 1.0f;
                            uv.X *= iResolution.X / iResolution.Y;
                            float time = (float)(rootConsole.TotalTime * 0.3f);

                            // ray
                            Vector3 ang = new Vector3(0.0f, 1.5f, 0.0f);
                            Vector3 ori = new Vector3(0.0f, 100f, 0f);
                            Vector3 dir = Vector3.Normalize(new Vector3(uv.X, uv.Y, -2.0f));
                            float UVLength = (float)Math.Sqrt(uv.X * uv.X + uv.Y * uv.Y);
                            dir.Z += UVLength * 0.15f;
                            dir = mult(Vector3.Normalize(dir), fromEuler(ang));

                            // tracing
                            Vector3 p = new Vector3();
                            heightMapTracing(ori, dir, ref p);
                            Vector3 dist = p - ori;
                            dist = new Vector3(dist.X - 0.8f, dist.Y - 0.8f, dist.Z - 0.8f);
                            Vector3 n = getNormal(p, Vector3.Dot(dist, dist) * (float)EPSILON_NRM);
                            Vector3 light = Vector3.Normalize(new Vector3(0.0f, 1.0f, 0.8f));

                            // color
                            Vector3 color = mix( //new Vector3(0.4f,0.4f,0.4f), 
                                getSkyColor(dir),
                                getSeaColor(p, n, light, dir, dist),
                                (float)Math.Pow(smoothstep(0.0f, -0.05f, dir.Y), 0.3f));

                            // post
                            fragColor = new Vector3((float)Math.Pow(color.X, 0.75f), (float)Math.Pow(color.Y, 0.75f), (float)Math.Pow(color.Z, 0.75f));
                            int localX = ix%cTile.Width;
                            int localY = iy%cTile.Height;
                            if (localX < 0)
                                localX += cTile.Width;
                            if (localY < 0)
                                localY += cTile.Height;
                            RLColor colorNew = new RLColor(fragColor.X, fragColor.Y, fragColor.Z);
                            cTile.Model.renderedCells[localX, localY].backColor = colorNew;
                            cTile.Model.renderedCells[localX, localY].color = colorNew;
                            cTile.Model.ModelChanged = true;
                        }
                    }
                   // rootConsole.DrawDynamicObject(cTile.Model, new Vector2(x,y), cTransform.Angle, camera.position, cTile.Model.ModelChanged);
                }               
            }
        }

        public SOceanAnimatorSeascape(Type requiredType, params Type[] otherTypes) : base(requiredType, otherTypes)
        {
        }

        public SOceanAnimatorSeascape() : base(Aspect.All(typeof(CTile),typeof(CTransform)))
        {
        }
    }
}
