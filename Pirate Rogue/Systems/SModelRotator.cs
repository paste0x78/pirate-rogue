﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using Artemis;
using Artemis.Manager;
using Artemis.System;
using NLog;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using Pirate_Rogue.Templates;
using RLNET;
namespace Pirate_Rogue.Systems
{
    public class SModelRotator : EntityComponentProcessingSystem<CTransform, CWindCollider>
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        private static Logger logger;

        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof (RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof (Camera).ToString());
            if (logger == null)
                logger = LogManager.GetLogger(this.GetType().Name);
        }


        public override void Process(Entity entity, CTransform cTransform, CWindCollider cWindCollider)
        {
            if (!cTransform.AngleChanged)
                return;

            RLDynamicObject model = cWindCollider.WholeModel;
            Globals.MathHelpers.RotateModel(ref model, cTransform.Angle);
            float cellsWidth = model.rotatedCells.GetLength(0);
            float cellsHeight = model.rotatedCells.GetLength(1);
            cWindCollider.collisionRotatedModelTopLeft = new Vector2(float.MaxValue, float.MaxValue);
            cWindCollider.collisionRotatedModelBottomLeft = new Vector2(float.MaxValue, float.MinValue);
            cWindCollider.collisionRotatedModelTopRight = new Vector2(float.MinValue, float.MaxValue);
            cWindCollider.collisionRotatedModelBottomRight = new Vector2(float.MinValue, float.MinValue);
            for (int x = 0; x < cellsWidth; x++)
            {
                for (int y = 0; y < cellsHeight; y++)
                {
                    if (model.rotatedCells[x, y].character > 1)
                    {
                        if (x < cWindCollider.collisionRotatedModelTopLeft.X)
                            cWindCollider.collisionRotatedModelTopLeft.X = x;
                        if (x < cWindCollider.collisionRotatedModelBottomLeft.X)
                            cWindCollider.collisionRotatedModelBottomLeft.X = x;
                        if (x > cWindCollider.collisionRotatedModelBottomRight.X)
                            cWindCollider.collisionRotatedModelBottomRight.X = x;
                        if (x > cWindCollider.collisionRotatedModelTopRight.X)
                            cWindCollider.collisionRotatedModelTopRight.X = x;

                        if (y < cWindCollider.collisionRotatedModelTopLeft.Y)
                            cWindCollider.collisionRotatedModelTopLeft.Y = y;
                        if (y < cWindCollider.collisionRotatedModelTopRight.Y)
                            cWindCollider.collisionRotatedModelTopRight.Y = y;
                        if (y > cWindCollider.collisionRotatedModelBottomLeft.Y)
                            cWindCollider.collisionRotatedModelBottomLeft.Y = y;
                        if (y > cWindCollider.collisionRotatedModelBottomRight.X)
                            cWindCollider.collisionRotatedModelBottomRight.Y = y;
                    }
                }
            }
            if (cWindCollider.SailsLength < 0)
            {
                cWindCollider.collisionModelTopLeft = new Vector2(float.MaxValue, float.MaxValue);
                cWindCollider.collisionModelBottomLeft = new Vector2(float.MaxValue, float.MinValue);
                cWindCollider.collisionModelTopRight = new Vector2(float.MinValue, float.MaxValue);
                cWindCollider.collisionModelBottomRight = new Vector2(float.MinValue, float.MinValue);
                for (int x = 0; x < model.renderedCells.GetLength(0); x++)
                {
                    for (int y = 0; y < model.renderedCells.GetLength(1); y++)
                    {
                        if (model.renderedCells[x, y].character > 1)
                        {
                            if (x < cWindCollider.collisionModelTopLeft.X)
                                cWindCollider.collisionModelTopLeft.X = x;
                            if (x < cWindCollider.collisionModelBottomLeft.X)
                                cWindCollider.collisionModelBottomLeft.X = x;
                            if (x > cWindCollider.collisionModelBottomRight.X)
                                cWindCollider.collisionModelBottomRight.X = x;
                            if (x > cWindCollider.collisionModelTopRight.X)
                                cWindCollider.collisionModelTopRight.X = x;

                            if (y < cWindCollider.collisionModelTopLeft.Y)
                                cWindCollider.collisionModelTopLeft.Y = y;
                            if (y < cWindCollider.collisionModelTopRight.Y)
                                cWindCollider.collisionModelTopRight.Y = y;
                            if (y > cWindCollider.collisionModelBottomLeft.Y)
                                cWindCollider.collisionModelBottomLeft.Y = y;
                            if (y > cWindCollider.collisionModelBottomRight.X)
                                cWindCollider.collisionModelBottomRight.Y = y;
                        }
                    }
                }
                float farleftX = float.MaxValue;
                float farRightX = float.MinValue;

                if (cWindCollider.collisionModelTopLeft.X < farleftX)
                    farleftX = cWindCollider.collisionModelTopLeft.X;
                if (cWindCollider.collisionModelBottomLeft.X < farleftX)
                    farleftX = cWindCollider.collisionModelBottomLeft.X;
                if (cWindCollider.collisionModelBottomRight.X < farleftX)
                    farleftX = cWindCollider.collisionModelBottomRight.X;
                if (cWindCollider.collisionModelTopRight.X < farleftX)
                    farleftX = cWindCollider.collisionModelTopRight.X;

                if (cWindCollider.collisionModelTopLeft.X > farRightX)
                    farRightX = cWindCollider.collisionModelTopLeft.X;
                if (cWindCollider.collisionModelBottomLeft.X > farRightX)
                    farRightX = cWindCollider.collisionModelBottomLeft.X;
                if (cWindCollider.collisionModelBottomRight.X > farRightX)
                    farRightX = cWindCollider.collisionModelBottomRight.X;
                if (cWindCollider.collisionModelTopRight.X > farRightX)
                    farRightX = cWindCollider.collisionRotatedModelTopRight.X;


               cWindCollider.SailsLength = Math.Abs(farRightX - farleftX);
                cWindCollider.MaxSailsLength = cWindCollider.SailsLength;
            }
            cTransform.AngleChanged = false;
        }


    }
}

