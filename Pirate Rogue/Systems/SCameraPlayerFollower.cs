﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using Artemis;
using Artemis.System;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using RLNET;

namespace Pirate_Rogue.Systems
{
    class SCameraPlayerFollower : TagSystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
        }

        public SCameraPlayerFollower(string tag) : base(tag)
        {
        }

        public override void Process(Entity entity)
        {
            
            CTransform cTransform = entity.GetComponent<CTransform>();
/*            RLDynamicObject cM = entity.GetComponent<CModel>().Model;
            float playerWidth = cM.renderedCells.GetLength(0)/2f;
            float playerHeight = cM.renderedCells.GetLength(1)/2f;*/
            CVelocity cVelocity = entity.GetComponent<CVelocity>();

            Globals.Bounds playerBounds = new Globals.Bounds();
            playerBounds.min = cTransform.Position - new Vector2((rootConsole.Width * camera.ZoomOut / 2f), rootConsole.Height * camera.ZoomOut / 2f);
            playerBounds.max = playerBounds.min;
            camera.Update(playerBounds, cVelocity.Velocity, (float)rootConsole.DeltaInSeconds,cTransform.Angle);
            //camera.position = cTransform.Position - new Vector2(rootConsole.Width/2f-playerWidth, rootConsole.Height/2f-playerHeight);
        }
    }
}
