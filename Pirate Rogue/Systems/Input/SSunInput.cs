﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using RLNET;

namespace Pirate_Rogue.Systems.Input
{
    class SSunInput : TagSystem
    {
        private RLRootConsole rootConsole;

        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
        }

        public SSunInput(string tag) : base(tag)
        {
            
        }
        public override void Process(Entity entity)
        {
            if (!entity.HasComponent<CSun>())
                return;
            CSun cSun = entity.GetComponent<CSun>();
            foreach (RLKeyEvent key in rootConsole.Keyboard.PollKeyEvents())
            {
                /*                if (!key.Pressed)
                                { */
                switch (key.Key)
                {
                    case RLKey.PageUp:
                        cSun.SunValue += 0.01f;
                        break;
                    case RLKey.PageDown:
                        cSun.SunValue -= 0.01f;
                        break;
                }


            }

        }
    }
}
