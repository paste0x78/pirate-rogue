﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using Artemis;
using Artemis.System;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Globals;
using Pirate_Rogue.Singletones;
using RLNET;

namespace Pirate_Rogue.Systems
{
    class SPlayerInput : TagSystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        //TODO: move to component
        private int res = 0;
        private bool pause;

        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
        }

        public SPlayerInput(string tag) : base(tag)
        {
        }

        public override void Process(Entity entity)
        {
            if (!entity.HasComponent<CTransform>() || !entity.HasComponent<CVelocity>())
                return;
            CTransform cTransform = entity.GetComponent<CTransform>();
            CVelocity cVelocity = entity.GetComponent<CVelocity>();
            bool moved = false;
            float acceleration = 20f;

            cVelocity.Velocity = Vector2.Zero;
            foreach (RLKeyEvent key in rootConsole.Keyboard.PollKeyEvents())
            {
/*                if (!key.Pressed)
                {
                    switch (key.Char)
                    {
                        case '1':
                            switch (res)
                            {
                                case 0:                                  
                                    rootConsole.ResizeWindow(1024, 768);
                                    camera.position+=new Vector2(1024-800,768-600)/2;
                                    res++;
                                    break;
                                case 1:
                                    rootConsole.ResizeWindow(800, 600);
                                    camera.position -= new Vector2(1024 - 800, 768 - 600) / 2;
                                    res--;
                                    break;
                            }
                            break;

                    }
                    continue;
                }*/
                if (key.Key == ChangableValues.InputSettings.SetSailsDown)
                {
                    moved = true;
                    cVelocity.Velocity += new Vector2(0, (float)(acceleration * rootConsole.DeltaInSeconds));
/*                    cWindCollider.SailsLength -= 1.5f*(float) rootConsole.DeltaInSeconds;
                    if (cWindCollider.SailsLength < 0)
                        cWindCollider.SailsLength = 0;*/
                }
                if (key.Key == ChangableValues.InputSettings.SetSailsUp)
                {
                    moved = true;
                    cVelocity.Velocity += new Vector2(0, (float)(-acceleration * rootConsole.DeltaInSeconds));
/*                    cWindCollider.SailsLength += 1.5f*(float) rootConsole.DeltaInSeconds;
                    if (cWindCollider.SailsLength > cWindCollider.MaxSailsLength)
                        cWindCollider.SailsLength = cWindCollider.MaxSailsLength;*/
                }
                if (key.Key == ChangableValues.InputSettings.RotateShipLeft)
                {
                    moved = true;
                    cVelocity.Velocity += new Vector2((float)(-acceleration * rootConsole.DeltaInSeconds), 0);
//                    cTransform.Angle -= (float) rootConsole.DeltaInSeconds*25f;
//                    if (cTransform.Angle < 0)
//                        cTransform.Angle += 360;
//                    cTransform.Angle %= 360;
                }
                if (key.Key == ChangableValues.InputSettings.RotateShipRight)
                {
                    moved = true;
                    cVelocity.Velocity += new Vector2((float)(acceleration * rootConsole.DeltaInSeconds), 0);
/*                    cTransform.Angle += (float) rootConsole.DeltaInSeconds*25f;
                    if (cTransform.Angle < 0)
                        cTransform.Angle += 360;
                    cTransform.Angle %= 360;*/
                }
                if (key.Key == ChangableValues.InputSettings.RotateSailsLeft)
                {
                    cTransform.Angle -= (float)rootConsole.DeltaInSeconds * 25f;
                    if (cTransform.Angle < 0)
                        cTransform.Angle += 360;
                    cTransform.Angle %= 360;
/*                    cWindCollider.AngleOffset -= (float) rootConsole.DeltaInSeconds*5f;
                    if (cWindCollider.AngleOffset < -20f)
                        cWindCollider.AngleOffset = -20;*/
                }
                if (key.Key == ChangableValues.InputSettings.RotateSailsRight)
                {
                    cTransform.Angle += (float)rootConsole.DeltaInSeconds * 25f;
                    if (cTransform.Angle < 0)
                        cTransform.Angle += 360;
                    cTransform.Angle %= 360;
/*                    cWindCollider.AngleOffset += (float) rootConsole.DeltaInSeconds*5f;
                    if (cWindCollider.AngleOffset > 20f)
                        cWindCollider.AngleOffset = 20f;*/
                }
            }

            float maxSpeed = 5f;
            if (cVelocity.Velocity.X < -maxSpeed)
                cVelocity.Velocity.X = -maxSpeed;
            else if (cVelocity.Velocity.X > maxSpeed)
                cVelocity.Velocity.X = maxSpeed;
            if (cVelocity.Velocity.Y < -maxSpeed)
                cVelocity.Velocity.Y = -maxSpeed;
            else if (cVelocity.Velocity.Y > maxSpeed)
                cVelocity.Velocity.Y = maxSpeed;
        }
    }
}
