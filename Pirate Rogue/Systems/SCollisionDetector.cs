﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Artemis;
using Artemis.Manager;
using Artemis.System;
using NLog;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Components.Flags;
using Pirate_Rogue.Singletones;
using Pirate_Rogue.Templates;
using RLNET;
namespace Pirate_Rogue.Systems
{
    class SCollisionDetector : EntityComponentProcessingSystem<CTransform,CCollider>
    {
        public override void Process(Entity entity, CTransform component1, CCollider component2)
        {
            
        }

        protected override void ProcessEntities(IDictionary<int, Entity> entities)
        {
            List<Entity> entitiesToCheck = new List<Entity>(entities.Values);
            List<Entity> ignoreEntities = new List<Entity>();
            //TODO: make this tree based
            foreach (Entity entity1 in entitiesToCheck)
            {
/*                if(ignoreEntities.Contains(entity1))
                    continue;*/
                bool foundCollision = false;
                CCollider collider1 = entity1.GetComponent<CCollider>();
                CTransform cTransform1 = entity1.GetComponent<CTransform>();
                Vector2 vecGlobal1 = cTransform1.Position;
                foreach (Entity entity2 in entitiesToCheck)
                {
                    if(entity2.Id==entity1.Id)
                        continue;
/*                    if (ignoreEntities.Contains(entity2))
                        continue;*/
                    //TODO: add support for angled points
                    CCollider collider2 = entity2.GetComponent<CCollider>();
                                    CTransform cTransform2 = entity2.GetComponent<CTransform>();
                    Vector2 vecGlobal2 = cTransform2.Position;
                    bool found = false;
                    foreach (var colPoint1 in collider1.CollisionDictionary)
                    {
                        if (found)
                            break;
                        Vector2 globalPoint1 = colPoint1.Key + vecGlobal1;
                        foreach (var colPoint2 in collider2.CollisionDictionary)
                        {
                            Vector2 globalPoint2 = colPoint2.Key + vecGlobal2;
                            if (globalPoint1 == globalPoint2)
                            {
                                found = true;
                                foundCollision = true;
                                ignoreEntities.Add(entity1);
                                ignoreEntities.Add(entity2);
                                break;
                            }
                        }
                    }
                }
                if (!foundCollision)
                {
                    if (entity1.HasComponent<CIsColliding>())
                    {
                        entity1.RemoveComponent<CIsColliding>();
                    }
                }
                else
                {
                    if (!entity1.HasComponent<CIsColliding>())
                    {
                        entity1.AddComponent<CIsColliding>(new CIsColliding());
                    }                   
                }
            }
        }
        //TODO: make broad collision step with checking **ANGLED** rectangles
/*        private bool BroadCollisionExists(Vector2 rectALeftTop, Vector2 rectARightDown,
            Vector2 rectBLeftTop, Vector2 rectBRightDown)

        {
            return true;
        }*/
    }
}
