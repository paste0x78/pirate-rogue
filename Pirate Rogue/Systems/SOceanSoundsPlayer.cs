﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;

namespace Pirate_Rogue.Systems
{
    class SOceanSoundsPlayer : EntityComponentProcessingSystem<CSound>
    {
        public override void Process(Entity entity, CSound cSound)
        {
            string id = "test";
            if (AudioPlayer.HaveLooped2DSound(id))
                return;
            AudioPlayer.StartPlayingLooped2DSound(cSound.WaveSource,0.25f,id);
        }

        
    }
}
