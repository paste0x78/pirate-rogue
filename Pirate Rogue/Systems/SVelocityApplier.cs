﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using Artemis;
using Artemis.System;
using NLog;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Components.Flags;
using Pirate_Rogue.Singletones;
using RLNET;

namespace Pirate_Rogue.Systems
{
    class SVelocityApplier : EntityProcessingSystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        private static Logger logger;

        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            if (logger == null)
                logger = LogManager.GetLogger(this.GetType().Name);
        }

        public void Process(Entity entity, CVelocity cVelocity, CTransform cTransform)
        {
            Vector2 vel = cVelocity.Velocity*(float) rootConsole.DeltaInSeconds;

            if (Math.Abs(vel.X)<=0.0001f)
                vel.X = 0;
            if (Math.Abs(vel.Y) <= 0.0001f)
                vel.Y = 0;

            cTransform.Position += vel; //angleVel;
        }

        public SVelocityApplier() : base(Aspect.All(typeof(CVelocity),typeof(CTransform)).GetExclude(typeof(CIsColliding)))
        {
        }

        public SVelocityApplier(Type requiredType, params Type[] otherTypes) : base(requiredType, otherTypes)
        {
        }

        public override void Process(Entity entity)
        {
            CVelocity cVelocity = entity.GetComponent<CVelocity>();
            CTransform cTransform = entity.GetComponent<CTransform>();
            Process(entity,cVelocity,cTransform);
        }
    }
}
