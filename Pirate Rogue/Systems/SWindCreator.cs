﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using Artemis;
using Artemis.Manager;
using Artemis.System;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using Pirate_Rogue.Templates;
using RLNET;
namespace Pirate_Rogue.Systems
{
    public class SWindCreator : IntervalTagSystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;

        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
        }

        public SWindCreator(TimeSpan timeSpan, string tag) : base(timeSpan, tag)
        {
        }

        public override void Process(Entity entity)
        {
         //   this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            CWindAndOceanSettings windAndOceanSettings = entity.GetComponent<CWindAndOceanSettings>();
            if (windAndOceanSettings == null)
                return;
            Vector2 startPoint = camera.position+new Vector2(0,rootConsole.Height);
            for (int i = 0; i < rootConsole.Width; i++)
            {
                startPoint.X +=1;
                //EntityCreator.CreateWindRaycast(entityWorld,rootConsole, startPoint, windSettings.Angle-180, windSettings.DistanceToTravelInCells);
            }
        }
    }
}
