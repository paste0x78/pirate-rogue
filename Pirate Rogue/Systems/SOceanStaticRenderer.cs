﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading;
using NLog;
using OpenTK;
using OpenTK.Graphics.ES20;
using RLNET;

namespace Pirate_Rogue.Systems
{
    class SOceanStaticRenderer : EntityComponentProcessingSystem<CWindAndOceanSettings>
    {

        private RLRootConsole rootConsole;
        private static ILogger logger;
        private Camera camera;
        private Ocean ocean;
        private float[,] heightTable;



        public SOceanStaticRenderer()
        {
            if (logger == null)
            {
                logger = LogManager.GetCurrentClassLogger();
            }

        }


        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            this.ocean = EntitySystem.BlackBoard.GetEntry<Ocean>(typeof(Ocean).ToString());
        }



        public override void Process(Entity entity, CWindAndOceanSettings settings)
        {
/*            Entity playerEntity = entityWorld.TagManager.GetEntity(Globals.EnitityTags.Player.ToString());
            CModel playerModel = playerEntity.GetComponent<CModel>();
            CTransform playerTransform = playerEntity.GetComponent<CTransform>();
            Vector2 playerMin = playerTransform.Position;
            Vector2 playerMax = playerMin +
                                new Vector2(playerModel.Model.cells.GetLength(0),
                                    playerModel.Model.cells.GetLength(1));*/
            //TODO: think about reusing last frame, need to study how whole loop behaves (worth recording in this)
            //TODO: or maybe record every setting and wind instead generting this runtime?
            RLCell cell = new RLCell(settings.OceanColor,settings.OceanColor,219);
            //CVelocity playerVelocity = entityWorld.TagManager.GetEntity(Globals.EnitityTags.Player.ToString()).GetComponent<CVelocity>();
            rootConsole.ClearGameScreenBuffer(cell);
            Vector2 windAngleVector =
                Globals.MathHelpers.AngularVectorForGivenDegrees(settings.Angle);
            Vector2 windspeed = settings.Knots *3* new Vector2(windAngleVector.X,-windAngleVector.Y);
            CSun cSunSettings = entityWorld.TagManager.GetEntity(Globals.EnitityTags.SunSettings.ToString()).GetComponent<CSun>();
            if (windspeed != ocean.m_windSpeed)
            {
                ocean.m_windSpeed = windspeed;
                ocean.Start();
                //TODO: allow for smooth change of wind
            }
            ocean.EvaluateWavesFFT((float)rootConsole.TotalTime);
            float lowestHeight = float.MaxValue;
            float highestHeight = float.MinValue;

            heightTable = new float[(int) ocean.m_length, (int) ocean.m_length];
            foreach (Vector3 vertices in ocean.m_vertices)
            {
                if (vertices.Y > highestHeight)
                    highestHeight = vertices.Y;
                if (vertices.Y < lowestHeight)
                    lowestHeight = vertices.Y;

            }
            float revLowest = lowestHeight * -1f;
            float distance = highestHeight + revLowest;
/*            rootConsole.RemoveLastShaderArgumentFromScreenBuffer();
            rootConsole.AddShaderArgumentToGameScreenBuffer(shader =>
            {
                GL.Uniform1(GL.GetUniformLocation(shader.ShaderProgramID, "minHeight"), lowestHeight);
                GL.Uniform1(GL.GetUniformLocation(shader.ShaderProgramID, "maxHeight"), distance);
            });*/
            //logger.Debug("low:"+lowestHeight+";high:"+highestHeight);


            int len = (int) (ocean.m_length);

            int offsetX = (int)Math.Round(camera.position.X / ocean.m_length);
            int offsetY = (int)Math.Round(camera.position.Y / ocean.m_length);
            int zoomOffset = (int) (camera.ZoomOut *6f);

            foreach (Vector3 vertices in ocean.m_vertices)
            {

                int x = (int)Math.Round(vertices.X);
                int y = (int)Math.Round(vertices.Z);

                float value = vertices.Y;
                float posValue = value + revLowest;
                int whole = (int)Math.Floor(posValue);
                float remainder = posValue - whole;
                int lastElem = Globals.ReadOnlyValues.WaterColors.Length - 1;
                whole = MathHelper.Clamp(whole, 0, lastElem);
                cell.color = Globals.ReadOnlyValues.WaterColors[whole];

                //float mult = (value + revLowest) / distance;
                float mult = 1f-1f*((1f-remainder)/3f);
                cell.color *= (mult);
                cell.color *= 2f*cSunSettings.SunValue;
                
                if (whole >= lastElem -2)
                {
                    /*if (RandomHelpers.Random.NextDouble() >= 0.7)
                    {
                        cell.color = Globals.Values.WaterColors[lastElem];
                    }*/
                }

                for (int i = offsetX - zoomOffset; i < offsetX + zoomOffset; i++)
                {
                    for (int j = offsetY - zoomOffset; j < offsetY + zoomOffset; j++)
                    {
                        int realX = (int) (x + i*len - camera.position.X);
                        int realY =(int) (y + j*len - camera.position.Y);

 /*                       if (realX + 1 == playerMin.X && realY >= playerMin.Y && realY < playerMax.Y)
                        {

                            cell.color *= 2.5f;
                        }*/
                        rootConsole.SetGameScreenBuffer(realX, realY, cell);

                    }

                }

            }
        }
    }



 

}



















