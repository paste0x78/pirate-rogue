﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using System.Collections.Generic;
using RLNET;
using OpenTK;

namespace Pirate_Rogue.Systems
{
    class SModelRenderer : EntityComponentProcessingSystem<CModel, CTransform>
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        private RLColor transparentColor;
        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            this.transparentColor = Globals.ReadOnlyValues.TransparentColor;
        }


        public override void Process(Entity entity, CModel cModel, CTransform cTransform)
        {
            //TODO: make this as event to generate this once every sun change
            CSun cSunSettings = entityWorld.TagManager.GetEntity(Globals.EnitityTags.SunSettings.ToString()).GetComponent<CSun>();
            RLColor transparent1 = Globals.ReadOnlyValues.TransparentColor;
            RLColor transparent2 = Globals.ReadOnlyValues.TransparentColor2;
            cModel.Model.ModelChanged = true;
            //TODO: move this to another system?
            for (int x = 0; x < cModel.Model.renderedCells.GetLength(0); x++)
            {
                for (int y = 0; y < cModel.Model.renderedCells.GetLength(1); y++)
                {
                    RLCell cell = cModel.Model.unalteredCells[x, y];
                    RLColor color = new RLColor(cell.color);
                    if (color != transparent1 && color != transparent2)
                    {
                        cModel.Model.renderedCells[x, y].color = color*(1f + (cSunSettings.SunValue - 1.5f));
                    }
                    RLColor backColor = new RLColor(cell.backColor);
                    if (backColor != transparent1 && backColor != transparent2)
                    {
                        
                        cModel.Model.renderedCells[x, y].backColor = backColor * (1f + (cSunSettings.SunValue - 1.5f));
                    }
                }
            }
            rootConsole.DrawDynamicObject(transparentColor,cModel.Model,cTransform.Position,cTransform.Angle, camera.position);
        }


    }
}
