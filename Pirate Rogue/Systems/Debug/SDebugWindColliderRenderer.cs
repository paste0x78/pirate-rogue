﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using RLNET;


namespace Pirate_Rogue.Systems
{
    class SDebugWindColliderRenderer : EntityComponentProcessingSystem<CWindCollider,CTransform>
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        private RLColor transparentColor;
        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            this.transparentColor = Globals.ReadOnlyValues.TransparentColor;
        }

        public override void Process(Entity entity, CWindCollider cWind, CTransform cTransform)
        {
            if (!Globals.ChangableValues.PerformanceSettings.DebugMode)
                return;
            cWind.WholeModel.ModelChanged = true;
            rootConsole.DrawDynamicObject(transparentColor,cWind.WholeModel, cTransform.Position+cWind.PositionOffset, cTransform.Angle+cWind.AngleOffset, 
                camera.position);
            cWind.WholeModel.ModelChanged = false;
        }
    }
}
