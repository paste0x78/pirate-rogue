﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using Artemis;
using Artemis.System;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using Pirate_Rogue.Templates;
using RLNET;


namespace Pirate_Rogue.Systems
{
    class SDebugGUIWindRenderer : TagSystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;

        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
        }

        public SDebugGUIWindRenderer(string tag) : base(tag)
        {
        }

        public override void Process(Entity entity)
        {
            if (!Globals.ChangableValues.PerformanceSettings.DebugMode)
                return;
            CWindAndOceanSettings windAndOceanSettings = entity.GetComponent<CWindAndOceanSettings>();
            if (windAndOceanSettings == null)
                return;
            //TODO: make those in separate console window: https://roguesharp.wordpress.com/2016/03/02/roguesharp-v3-tutorial-multiple-consoles/
            rootConsole.Print(0, 3, string.Format("Wind direction:{0}; Wind power:{1}", windAndOceanSettings.Angle,windAndOceanSettings.Knots),
                RLColor.LightBlue, RLColor.Black);
        }
    }
}
