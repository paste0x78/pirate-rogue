﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using System.Collections.Generic;
using RLNET;
using System;
using Artemis.Manager;

namespace Pirate_Rogue.Systems
{
    public class SDebugGUIFPSInfoRenderer : EntityComponentProcessingSystem<CFpsCounter>
    {
        private RLRootConsole rootConsole;
        private Camera camera;

        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
        }       

        public override void Process(Entity entity, CFpsCounter cFpsCounter)
        {
            if (!Globals.ChangableValues.PerformanceSettings.DebugMode)
                return;
            string text = string.Format("FPS: {0}", cFpsCounter.FPS);
            rootConsole.Print(rootConsole.Width - text.Length, 0, text,
                (cFpsCounter.FPS >= 50 ? RLColor.Green : (cFpsCounter.FPS >= 30 ? RLColor.Yellow : RLColor.Red)), RLColor.Black);
            string text2 = string.Format("DeltaTicks: {0}", entityWorld.Delta.ToString("0000000"));
            rootConsole.Print(rootConsole.Width - text2.Length, 1, text2,RLColor.Green, RLColor.Black);
            string text3 = string.Format("DeltaSeconds: {0}", rootConsole.DeltaInSeconds.ToString("##.000000"));
            rootConsole.Print(rootConsole.Width - text3.Length, 2, text3, RLColor.Green, RLColor.Black);
        }
    }
}
