﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using Artemis;
using Artemis.System;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using System.Collections.Generic;
using RLNET;
using System;
using Artemis.Manager;

namespace Pirate_Rogue.Systems
{
    public class SDebugGUIPlayerInfoRenderer : TagSystem
    {
        private RLRootConsole rootConsole;
        private Camera camera;




        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
        }
        
        public SDebugGUIPlayerInfoRenderer() : base(Globals.EnitityTags.Player.ToString())
        {

        }

        public override void Process(Entity entity)
        {
            if (!Globals.ChangableValues.PerformanceSettings.DebugMode)
            {
                rootConsole.Print(0, 0, string.Format("Press {0} to enable debug mode",
                    Globals.ChangableValues.InputSettings.DebugMode.ToString()),
                    RLColor.White, RLColor.Black);
                return;
            }
            if (!entity.HasComponent<CTransform>()||!entity.HasComponent<CVelocity>()||!entity.HasComponent<CWindCollider>())
                return;
            CTransform cTransform = entity.GetComponent<CTransform>();
            CVelocity cVelocity = entity.GetComponent<CVelocity>();
            CWindCollider cWindCollider = entity.GetComponent<CWindCollider>();
            rootConsole.Print(0, 0, string.Format("Camera:{0},{1}; Zoom out:{2}; Position:{3},{4}", 
                camera.position.X.ToString("##.00"), camera.position.Y.ToString("##.00"), camera.ZoomOut.ToString("##.000"), cTransform.Position.X.ToString("##.00"), 
                cTransform.Position.Y.ToString("##.00")),
                RLColor.White, RLColor.Black);
            rootConsole.Print(0, 1, string.Format("Angle: {0}; Velocity:{1},{2}; Sails:{3}",
            cTransform.Angle.ToString("##.00"), cVelocity.Velocity.X.ToString("##.0"), cVelocity.Velocity.Y.ToString("##.0"), (cWindCollider.SailsLength/cWindCollider.MaxSailsLength*100f)+"%"),RLColor.White, RLColor.Black);
            

        }
    }
}
