﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using Artemis;
using Artemis.System;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using RLNET;

namespace Pirate_Rogue.Systems
{
    public class SSailsAnimator : EntityComponentProcessingSystem<CSailsAnimation, CTransform, CWindCollider, CVelocity>
    {
        private RLRootConsole rootConsole;
        private Camera camera;
        private RLColor transparentColor;
        protected override void Begin()
        {
            base.Begin();
            this.rootConsole = EntitySystem.BlackBoard.GetEntry<RLRootConsole>(typeof(RLRootConsole).ToString());
            this.camera = EntitySystem.BlackBoard.GetEntry<Camera>(typeof(Camera).ToString());
            transparentColor = Globals.ReadOnlyValues.TransparentColor;
        }


        public override void Process(Entity entity, CSailsAnimation cSailsAnimation, CTransform cTransform, CWindCollider cWindCollider, CVelocity cVelocity)
        {            
            //TODO: make this as event to generate this once every sun change
            CSun cSunSettings = entityWorld.TagManager.GetEntity(Globals.EnitityTags.SunSettings.ToString()).GetComponent<CSun>();
            RLColor transparent1 = Globals.ReadOnlyValues.TransparentColor;
            RLColor transparent2 = Globals.ReadOnlyValues.TransparentColor2;
            float velocity = Math.Abs(cVelocity.Velocity.X);
            if (Math.Abs(cVelocity.Velocity.Y) > velocity)
                velocity = Math.Abs(cVelocity.Velocity.Y);
            int lastAnimation = cSailsAnimation.CurrentAnimation;
            //TODO: base this on knots?
            float border1 = 0.25f*cWindCollider.SailsLength/cWindCollider.MaxSailsLength;
            float border2 = 2.5f*cWindCollider.SailsLength/cWindCollider.MaxSailsLength;
            cSailsAnimation.CurrentAnimation = velocity <= border1 ? 0 : (velocity < border2 ? 1 : 2);
            if (lastAnimation != cSailsAnimation.CurrentAnimation)
                cSailsAnimation.CurrentFrame = 0;
            RLDynamicObject frame =
                cSailsAnimation.framesDictionary[cSailsAnimation.CurrentAnimation][cSailsAnimation.CurrentFrame];
            //TODO: move this to another system?
            frame.ModelChanged = true;
            for (int x = 0; x < frame.renderedCells.GetLength(0); x++)
            {
                for (int y = 0; y < frame.renderedCells.GetLength(1); y++)
                {
                    RLCell cell = frame.unalteredCells[x, y];
                    RLColor color = new RLColor(cell.color);
                    if (color != transparent1 && color != transparent2)
                    {
                        frame.renderedCells[x, y].color = color * (1f + (cSunSettings.SunValue - 1.5f));
                    }
                    RLColor backColor = new RLColor(cell.backColor);
                    if (backColor != transparent1 && backColor != transparent2)
                    {

                        frame.renderedCells[x, y].backColor = backColor * (1f + (cSunSettings.SunValue - 1.5f));
                    }
                }
            }
            rootConsole.DrawDynamicObject(transparentColor,frame, cTransform.Position, 
                cTransform.Angle, camera.position, false, cWindCollider.AngleOffset, cSailsAnimation.PositionOffset + cWindCollider.PositionOffset,new Vector3(1,cWindCollider.SailsLength/cWindCollider.MaxSailsLength,1));
            if (cSailsAnimation.Timer >= cSailsAnimation.SecondsBetweenFrames)
            {
                cSailsAnimation.Timer = 0;
                cSailsAnimation.CurrentFrame++;
                if (cSailsAnimation.CurrentFrame >= cSailsAnimation.framesDictionary[cSailsAnimation.CurrentAnimation].Count)
                    cSailsAnimation.CurrentFrame =cSailsAnimation.framesDictionary[cSailsAnimation.CurrentAnimation].Count-2;
                return;
            }
            cSailsAnimation.Timer += (float)rootConsole.DeltaInSeconds*10f;
        }
    }
}
