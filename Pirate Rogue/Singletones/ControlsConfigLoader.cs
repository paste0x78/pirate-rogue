﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using Pirate_Rogue.Globals;
using RLNET;

namespace Pirate_Rogue.Singletones
{
    public static class ControlsConfigLoader
    {
        public static void LoadConfigFile()
        {
            if (!SaveLoadManager.IsConfigFileExist(Globals.FilePaths.GetControlsConfigFilePath()))
                return;
            string val;
            foreach (var sel in Enum.GetValues(typeof(Globals.ConfigFileEnum)))
            {
                Globals.ConfigFileEnum en = (Globals.ConfigFileEnum) sel;
                switch (en)
                {

                    case Globals.ConfigFileEnum.RotateSailsLeft:
                        val = SaveLoadManager.LoadStringFromConfigFile(Globals.FilePaths.GetControlsConfigFilePath(),Globals.ConfigFileEnum.RotateSailsLeft);
                        if (!string.IsNullOrEmpty(val))
                        {
                            RLKey newKey = RLKey.Enter;
                            RLKey.TryParse(val, true, out newKey);
                            if (newKey != RLKey.Enter)
                            {
                                ChangableValues.InputSettings.RotateSailsLeft = newKey;
                            }
                        }
                        break;
                    case Globals.ConfigFileEnum.RotateSailsRight:
                        val = SaveLoadManager.LoadStringFromConfigFile(Globals.FilePaths.GetControlsConfigFilePath(),Globals.ConfigFileEnum.RotateSailsRight);
                        if (!string.IsNullOrEmpty(val))
                        {
                            RLKey newKey = RLKey.Enter;
                            RLKey.TryParse(val, true, out newKey);
                            if (newKey != RLKey.Enter)
                            {
                                ChangableValues.InputSettings.RotateSailsRight = newKey;
                            }
                        }
                        break;
                    case Globals.ConfigFileEnum.RotateShipLeft:
                        val = SaveLoadManager.LoadStringFromConfigFile(Globals.FilePaths.GetControlsConfigFilePath(),Globals.ConfigFileEnum.RotateShipLeft);
                        if (!string.IsNullOrEmpty(val))
                        {
                            RLKey newKey = RLKey.Enter;
                            RLKey.TryParse(val, true, out newKey);
                            if (newKey != RLKey.Enter)
                            {
                                ChangableValues.InputSettings.RotateShipLeft = newKey;
                            }
                        }
                        break;
                    case Globals.ConfigFileEnum.RotateShipRight:
                        val = SaveLoadManager.LoadStringFromConfigFile(Globals.FilePaths.GetControlsConfigFilePath(),Globals.ConfigFileEnum.RotateShipRight);
                        if (!string.IsNullOrEmpty(val))
                        {
                            RLKey newKey = RLKey.Enter;
                            RLKey.TryParse(val, true, out newKey);
                            if (newKey != RLKey.Enter)
                            {
                                ChangableValues.InputSettings.RotateShipRight = newKey;
                            }
                        }
                        break;
                    case Globals.ConfigFileEnum.SetSailsDown:
                        val = SaveLoadManager.LoadStringFromConfigFile(Globals.FilePaths.GetControlsConfigFilePath(),Globals.ConfigFileEnum.SetSailsDown);
                        if (!string.IsNullOrEmpty(val))
                        {
                            RLKey newKey = RLKey.Enter;
                            RLKey.TryParse(val, true, out newKey);
                            if (newKey != RLKey.Enter)
                            {
                                ChangableValues.InputSettings.SetSailsDown = newKey;
                            }
                        }
                        break;
                    case Globals.ConfigFileEnum.SetSailsUp:
                        val = SaveLoadManager.LoadStringFromConfigFile(Globals.FilePaths.GetControlsConfigFilePath(),Globals.ConfigFileEnum.SetSailsUp);
                        if (!string.IsNullOrEmpty(val))
                        {
                            RLKey newKey = RLKey.Enter;
                            RLKey.TryParse(val, true, out newKey);
                            if (newKey != RLKey.Enter)
                            {
                                ChangableValues.InputSettings.SetSailsUp = newKey;
                            }
                        }
                        break;
                    case Globals.ConfigFileEnum.ZoomIn:
                        val = SaveLoadManager.LoadStringFromConfigFile(Globals.FilePaths.GetControlsConfigFilePath(), Globals.ConfigFileEnum.ZoomIn);
                        if (!string.IsNullOrEmpty(val))
                        {
                            RLKey newKey = RLKey.Enter;
                            RLKey.TryParse(val, true, out newKey);
                            if (newKey != RLKey.Enter)
                            {
                                ChangableValues.InputSettings.ZoomIn = newKey;
                            }
                        }
                        break;
                    case Globals.ConfigFileEnum.ZoomOut:
                        val = SaveLoadManager.LoadStringFromConfigFile(Globals.FilePaths.GetControlsConfigFilePath(), Globals.ConfigFileEnum.ZoomOut);
                        if (!string.IsNullOrEmpty(val))
                        {
                            RLKey newKey = RLKey.Enter;
                            RLKey.TryParse(val, true, out newKey);
                            if (newKey != RLKey.Enter)
                            {
                                ChangableValues.InputSettings.ZoomOut = newKey;
                            }
                        }
                        break;
                }
            }

        }
    }
}
