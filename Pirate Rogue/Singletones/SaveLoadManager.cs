﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using NLog;

namespace Pirate_Rogue.Singletones
{
    public static class SaveLoadManager
    {
        //TODO: dont make this more general than needed, when it stops getting job done, move to FlatBuffers
        private static ILogger logger = LogManager.GetCurrentClassLogger();
        public static bool IsConfigFileExist(string path)
        {
            return File.Exists(path);
        }

        private static FileStream CreateEmptyConfigFile(string dirPath, string filePath)
        {
            if(!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);
            return File.Create(filePath);
        }


        public static string LoadStringFromConfigFile(string path, Globals.ConfigFileEnum position, string defaultValue = "")
        {
            if (!IsConfigFileExist(path))
                return defaultValue;
            string[] values = File.ReadAllLines(path);
            int id = Globals.EnumHelper.GetPositionOfEnumValue(position);
            if(id>=values.Length)
                return defaultValue;
            return values[id];
        }
        public static bool SaveStringToConfigFile(string filePath,string dirPath, Globals.ConfigFileEnum position, string value)
        {
            if (!IsConfigFileExist(filePath))
                CreateEmptyConfigFile(dirPath,filePath).Close();
            string[] values = File.ReadAllLines(Globals.FilePaths.GetControlsConfigFilePath());
            int id = Globals.EnumHelper.GetPositionOfEnumValue(position);
            string[] destValues;
            if (id >= values.Length)
            {
                destValues = new string[id + 1];
            }
            else
            {
                destValues = new string[values.Length];
            }
            Array.Copy(values, destValues, values.Length);
            destValues[id] = value;
            return SaveAllValuesToConfigFile(filePath,destValues);
        }

        private static bool SaveAllValuesToConfigFile(string path, string[] values)
        {
            File.WriteAllLines(path,values);
            return true;
        }
    }
}
