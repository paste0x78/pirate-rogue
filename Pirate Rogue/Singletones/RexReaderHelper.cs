﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RexTools;
using RLNET;

namespace Pirate_Rogue.Singletones
{
    public static class RexReaderHelper
    {
        
        public static List<RLDynamicObject> CutFramesFromModel(RLDynamicObject model, int width, int height, int line=-1)
        {
            List<RLDynamicObject> frames = new List<RLDynamicObject>();
            for (int x = 0; x < model.renderedCells.GetLength(0); x += width)
            {
                if (line < 0)
                {
                    for (int y = 0; y < model.renderedCells.GetLength(1); y += height)
                    {
                        RLDynamicObject obj = new RLDynamicObject(width, height);
                        obj.CreateShader(
                            System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphVertexShaderPath()),
                            System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphFragmentShaderPath()));
                        for (int ix = x; ix < x + width; ix++)
                        {
                            for (int iy = y; iy < y + height; iy++)
                            {
                                obj.renderedCells[ix%width, iy%height] = model.renderedCells[ix, iy];
                            }
                        }
                        obj.CreateUnalteredModel();
                        frames.Add(obj);
                    }
                }
                else
                {
                    for (int y = line* height; y < line* height + height; y += height)
                    {
                        RLDynamicObject obj = new RLDynamicObject(width, height);
                        obj.CreateShader(
                            System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphVertexShaderPath()),
                            System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphFragmentShaderPath()));
                        for (int ix = x; ix < x + width; ix++)
                        {
                            for (int iy = y; iy < y + height; iy++)
                            {
                                obj.renderedCells[ix % width, iy % height] = model.renderedCells[ix, iy];
                            }
                        }
                        obj.CreateUnalteredModel();
                        frames.Add(obj);
                    }
                }
            }
            return frames;

        }

        public static bool LoadXP(string path, bool colorEmpty, int layer, ref RLDynamicObject model)
        {
            bool loaded = false;
            int counter = 0;
            RexReader reader = null;
            while (!loaded&&counter<5)
            {
                try
                {
                    reader = new RexReader(path);
                }
                catch (Exception e)
                {
                    System.Threading.Thread.Sleep(250);
                    counter++;
                }
                loaded = true;
            }
            if (reader == null)
                return false;
            TileMap map = reader.GetMap();
            if (layer >= map.LayerCount)
                return false;
            Tile[,] tiles = map.Layers[layer].Tiles;
            int xLength = tiles.GetLength(1);
            int yLength = tiles.GetLength(0);
            model = new RLDynamicObject(xLength, yLength);
            model.CreateShader(System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphVertexShaderPath()), System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphFragmentShaderPath()));
            for (int x = 0; x < xLength; x++)
            {
                for (int y = 0; y < yLength; y++)
                {
                    Tile tile = tiles[y, x];
                    RLColor bg = new RLColor(tile.BackgroundRed, tile.BackgroundGreen, tile.BackgroundBlue);
                    RLColor fg = new RLColor(tile.ForegroundRed, tile.ForegroundGreen, tile.ForegroundBlue);
                    if (tile == null)
                    {
                        model.renderedCells[x, y].character = -1;
                        continue;
                    }
                    else if (fg==Globals.ReadOnlyValues.TransparentColor)
                    {
                            model.renderedCells[x, y].character = -1;
                            continue;
                    }


                    model.renderedCells[x, y].backColor = bg;
                    model.renderedCells[x, y].color = fg;
                    model.renderedCells[x, y].character = tile.CharacterCode;
                }
            }
            reader.Dispose();
            return true;
        }
   

    }
}
