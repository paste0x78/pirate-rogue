﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using CSCore;
using CSCore.SoundOut;
using NLog;

namespace Pirate_Rogue.Singletones
{
    public static class AudioPlayer
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        //TODO: add event to rescale volume everywhere
        public static float Volume { get; private set; }
        private static List<ISoundOut> players2DOneSounds = new List<ISoundOut>();
        private static Dictionary<string, ISoundOut> players2DLoopedSounds = new Dictionary<string, ISoundOut>();
        private static bool update=true;

        public static void StartPlayingLooped2DSound(IWaveSource sound, float soundVolume, string id)
        {
            //SoundOut implementation which plays the sound
            ISoundOut soundOut = GetSoundOut();
            {
                //Tell the SoundOut which sound it has to play
                soundOut.Initialize(sound);

                soundOut.Volume = Volume * GetVolumeValue(soundVolume);
                //Play the sound
                soundOut.Play();

                players2DLoopedSounds.Add(id,soundOut);
            }     
        }

        public static bool HaveLooped2DSound(string id)
        {
            return players2DLoopedSounds.ContainsKey(id);
        }
        public static void StopLooped2DSound(string id)
        {
            if (HaveLooped2DSound(id))
            {
                ISoundOut soundOut = players2DLoopedSounds[id];

                soundOut.Stop();
                players2DOneSounds.Remove(soundOut);
                soundOut.Dispose();
            }
        }
        public static void Play2DSoundOnce(IWaveSource sound, float soundVolume)
        {
            //SoundOut implementation which plays the sound
            ISoundOut soundOut = GetSoundOut();
            {
                //Tell the SoundOut which sound it has to play
                soundOut.Initialize(sound);

                soundOut.Volume = Volume * GetVolumeValue(soundVolume);
                //Play the sound
                soundOut.Play();

                players2DOneSounds.Add(soundOut);
            }           
        }

        public static void Update(double delta)
        {
            if (!update)
                return;
            for(int i=players2DOneSounds.Count-1;i>=0;i--)
            {
                ISoundOut soundOut = players2DOneSounds[i];
                if (soundOut.WaveSource.Position >= soundOut.WaveSource.Length)
                {
                    soundOut.Stop();
                    soundOut.Dispose();
                    players2DOneSounds.RemoveAt(i);
                }
            }
            foreach (KeyValuePair<string, ISoundOut> pair in players2DLoopedSounds)
            {
                ISoundOut soundOut = pair.Value;
                if (soundOut.WaveSource.Position >= soundOut.WaveSource.Length)
                {
                    soundOut.Stop();
                    soundOut.WaveSource.Position = 0;
                    soundOut.Play();
                }
            }
        }

        public static void Dispose()
        {
            update = false;

            foreach (KeyValuePair<string, ISoundOut> pair in players2DLoopedSounds)
            {
                ISoundOut soundOut = pair.Value;
                soundOut.Stop();
                soundOut.Dispose();
            }
            players2DLoopedSounds.Clear();
            players2DOneSounds.Clear();

        }
        private static ISoundOut GetSoundOut()
        {
            if (WasapiOut.IsSupportedOnCurrentPlatform)
                return new WasapiOut();
            return new DirectSoundOut();
        }
        public static void SetVolume(float value)
        {
            Volume = GetVolumeValue(value);
        }

        private static float GetVolumeValue(float value)
        {
            value = Math.Abs(value);
            if (value > 1f)
                value = 1f;
            return value;
        }
    }
}
