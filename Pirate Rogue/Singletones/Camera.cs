﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using System.Drawing;
using OpenTK;

namespace Pirate_Rogue.Singletones
{
    //TODO: make camera normal component as it would be good to have ability to have different cameras and some of them make static
    public class Camera
    {
        public Vector2 position { get; private set; }
        public float ZoomOut;
        private float maxZoomOut;
        public float MaxZoomOut
        {
            get { return maxZoomOut; }
            set
            {
                maxZoomOut = value;
                maxZoomOutForPlayer=maxZoomOut+(1-maxZoomOut)/2f;
            }
        }

    
        private float maxZoomOutForPlayer;
        public float MaxZoomOutForPlayer {
            get { return maxZoomOutForPlayer; }
        }
        private Globals.FocusArea focusArea;

        private bool lookAheadStoppedX;
        private bool lookAheadStoppedY;

        private float currentLookAheadX;
        private float targetLookAheadX;
        private float lookAheadDirX;
        private float smoothLookVelocityX;
        private float currentLookAheadY;
        private float targetLookAheadY;
        private float lookAheadDirY;
        private float smoothLookVelocityY;
    
        public float lookAheadDstX=15;
        public float lookSmoothTimeX=0.02f;
        public float lookAheadDstY = 15;
        public float lookSmoothTimeY = 0.02f;
        public float maxSpeed=50f;
        public Vector2 focusAreaSize = new Vector2(3f,3f);


        public Camera(float x, float y, Globals.Bounds targetBounds)
        {
            this.position = new Vector2(x, y);
            ZoomOut = 1f;

            focusArea = new Globals.FocusArea(targetBounds, focusAreaSize);
            MaxZoomOut = Globals.ChangableValues.PerformanceSettings.MaximumZoomOut;
        }

        public void Update(Globals.Bounds targetBounds, Vector2 targetVelocity, float delta, float shipDirection)
        {
            Vector2 up = new Vector2(0,1);
            Vector2 right = new Vector2(1,0);
            focusArea.Update(targetBounds);

            Vector2 focusPosition = focusArea.centre;

            targetLookAheadX = 0;
            targetLookAheadY = 0;
            Vector2 angularVelocity = Globals.MathHelpers.AngularVectorForGivenDegrees(shipDirection); 
            if (focusArea.velocity.X != 0)
            {
                lookAheadDirX = angularVelocity.X;//Math.Sign(focusArea.velocity.X);
                if (targetVelocity.X != 0)
                {
                    //TODO: make this based on angular velocity
                    lookAheadStoppedX = false;
                    targetLookAheadX = lookAheadDirX * lookAheadDstX;
                }
                else
                {
                    if (!lookAheadStoppedX)
                    {
                        lookAheadStoppedX = true;
                        targetLookAheadX = currentLookAheadX + (lookAheadDirX * lookAheadDstX - currentLookAheadX) / 4f;
                    }
                }
            }

            if (focusArea.velocity.Y != 0)
            {
                lookAheadDirY = angularVelocity.Y;//Math.Sign(focusArea.velocity.Y);
                if (targetVelocity.Y != 0)
                {
                    lookAheadStoppedY = false;
                    targetLookAheadY = lookAheadDirY * lookAheadDstY;
                }
                else
                {
                    if (!lookAheadStoppedY)
                    {
                        lookAheadStoppedY = true;
                        targetLookAheadY = currentLookAheadY + (lookAheadDirY * lookAheadDstY - currentLookAheadY) / 4f;
                    }
                }
            }

            currentLookAheadX = SmoothDamp(currentLookAheadX, targetLookAheadX, ref smoothLookVelocityX, lookSmoothTimeX, maxSpeed, delta);
            currentLookAheadY = SmoothDamp(currentLookAheadY, targetLookAheadY, ref smoothLookVelocityY, lookSmoothTimeY, maxSpeed, delta);

            //focusPosition.Y = SmoothDamp(position.Y, focusPosition.Y, ref smoothVelocityY, verticalSmoothTime, maxSpeed, delta);
            focusPosition += right * currentLookAheadX;
            focusPosition += up*currentLookAheadY;
            position = focusPosition;
        }
        public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
        {
            smoothTime = Math.Max(0.0001f, smoothTime);
            float num = 2f / smoothTime;
            float num2 = num * deltaTime;
            float num3 = 1f / (1f + num2 + 0.48f * num2 * num2 + 0.235f * num2 * num2 * num2);
            float num4 = current - target;
            float num5 = target;
            float num6 = maxSpeed * smoothTime;
            num4 = MathHelper.Clamp(num4, -num6, num6);
            target = current - num4;
            float num7 = (currentVelocity + num * num4) * deltaTime;
            currentVelocity = (currentVelocity - num * num7) * num3;
            float num8 = target + (num4 + num7) * num3;
            if (num5 - current > 0f == num8 > num5)
            {
                num8 = num5;
                currentVelocity = (num8 - num5) / deltaTime;
            }
            return num8;
        }
    }
}
