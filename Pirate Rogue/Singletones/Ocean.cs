﻿using System;
using System.Drawing;
using OpenTK;
using Pirate_Rogue.Globals;

namespace Pirate_Rogue.Singletones
{
    /// <summary>
    /// 
    /// https://scrawkblog.com/2013/08/04/ocean-waves-using-phillips-spectrum-in-unity/
    /// </summary>
    public class Ocean
    {
        const float GRAVITY = 9.81f;



        /// <summary>
        /// The fourier size. Must be a pow2 number.
        /// </summary>
        int N = 64;

        /// <summary>
        /// The world space length of each mesh.
        /// </summary>
        public float m_length = 32;

        /// <summary>
        /// The phillips spectrum parameter. Affects heights of waves.
        /// Setting this to hight can cause the waves to curl back
        /// on themselves.
        /// </summary>
        float m_waveAmp = 0.0005f;

        /// <summary>
        /// The wind speed and the direction.
        /// </summary>
        public Vector2 m_windSpeed = new Vector2(0f, 10f);


        /// <summary>
        /// Just the fourier size plus 1.
        /// </summary>
        int Nplus1;

        /// <summary>
        /// The wind direction.
        /// </summary>
        Vector2 m_windDirection;

        /// <summary>
        /// Use to perform the fourier transform.
        /// </summary>
        FourierCPU m_fourier;

        /// <summary>
        /// Buffers for fourier transform.
        /// </summary>
        Vector2[,] m_heightBuffer;
        Vector4[,] m_slopeBuffer, m_displacementBuffer;

        /// <summary>
        /// Holds the spectrum which will be then be transformed into
        /// the wave heights/slopes.
        /// </summary>
        Vector2[] m_spectrum, m_spectrum_conj;

        /// <summary>
        /// 
        /// </summary>
        Vector3[] m_position;
        public Vector3[] m_vertices;
        Vector3[] m_normals;

        /// <summary>
        /// Just holds so data for the spectrum that can be precomputed.
        /// </summary>
        float[] m_dispersionTable;

        /// <summary>
        /// The fresnel look up table.
        /// </summary>
        Bitmap m_fresnelLookUp;


        public void Start()
        {
            Nplus1 = N + 1;

            m_fourier = new FourierCPU(N);

            m_windDirection = new Vector2(m_windSpeed.X, m_windSpeed.Y);
            m_windDirection.Normalize();

            m_dispersionTable = new float[Nplus1 * Nplus1];

            for (int m_prime = 0; m_prime < Nplus1; m_prime++)
            {
                for (int n_prime = 0; n_prime < Nplus1; n_prime++)
                {
                    int index = m_prime * Nplus1 + n_prime;
                    m_dispersionTable[index] = Dispersion(n_prime, m_prime);
                }
            }

            m_heightBuffer = new Vector2[2, N * N];
            m_slopeBuffer = new Vector4[2, N * N];
            m_displacementBuffer = new Vector4[2, N * N];

            m_spectrum = new Vector2[Nplus1 * Nplus1];
            m_spectrum_conj = new Vector2[Nplus1 * Nplus1];

            m_position = new Vector3[Nplus1 * Nplus1];
            m_vertices = new Vector3[Nplus1 * Nplus1];
            m_normals = new Vector3[Nplus1 * Nplus1];


            Vector3[] vertices = new Vector3[Nplus1 * Nplus1];
            for (int x = 0; x < Nplus1; x++)
            {
                for (int y = 0; y < Nplus1; y++)
                {
                    Vector3 pos = new Vector3(x, 0.0f, y);

                    vertices[x + y * Nplus1] = pos;
                }
            }

            for (int m_prime = 0; m_prime < Nplus1; m_prime++)
            {
                for (int n_prime = 0; n_prime < Nplus1; n_prime++)
                {
                    int index = m_prime * Nplus1 + n_prime;

                    m_spectrum[index] = GetSpectrum(n_prime, m_prime);

                    m_spectrum_conj[index] = GetSpectrum(-n_prime, -m_prime);
                    m_spectrum_conj[index].Y *= -1.0f;

                    m_position[index].X = vertices[index].X = n_prime * m_length / N;
                    m_position[index].Y = vertices[index].Y = 0.0f;
                    m_position[index].Z = vertices[index].Z = m_prime * m_length / N;

                }
            }


           //  CreateFresnelLookUp();
        }

        /// <summary>
        /// Create a fresnel lookup table. This is the formula
        /// to calculate a materials fresnel value based on 
        /// its refractive index. Since its a little math heavy
        /// a look up table is used rather than caculate it in 
        /// the shader. In practise this method does not look any better
        /// than cheaper approximations but is included out of interest.
        /// </summary>
        void CreateFresnelLookUp()
        {
            float nSnell = 1.34f; //Refractive index of water

            m_fresnelLookUp = new Bitmap(512, 1);/*
            m_fresnelLookUp.filterMode = FilterMode.Bilinear;
            m_fresnelLookUp.WrapMode = TextureWrapMode.Clamp;
            m_fresnelLookUp.anisoLevel = 0;*/

            for (int x = 0; x < 512; x++)
            {
                double fresnel = 0.0f;
                double costhetai = (float)x / 511.0f;
                double thetai = Math.Acos(costhetai);
                double sinthetat = Math.Sin(thetai) / nSnell;
                double thetat = Math.Asin(sinthetat);

                if (thetai == 0.0f)
                {
                    fresnel = (nSnell - 1.0f) / (nSnell + 1.0f);
                    fresnel = fresnel * fresnel;
                }
                else
                {
                    float fs = (float)(Math.Sin(thetat - thetai) / Math.Sin(thetat + thetai));
                    float ts = (float)(Math.Tan(thetat - thetai) / Math.Tan(thetat + thetai));
                    fresnel = 0.5f * (fs * fs + ts * ts);
                }

                m_fresnelLookUp.SetPixel(x, 0, Color.FromArgb((int)(fresnel / 255f), (int)(fresnel / 255f), (int)(fresnel / 255f), (int)(fresnel / 255f)));
            }

            //m_fresnelLookUp.Apply();

            // m_mat.SetTexture("_FresnelLookUp", m_fresnelLookUp);
        }


        /// <summary>
        /// Gets the spectrum vaule for grid position n,m. 
        /// </summary>
        Vector2 GetSpectrum(int n_prime, int m_prime)
        {
            Vector2 r = GaussianRandomVariable();
            return r * (float)Math.Sqrt(PhillipsSpectrum(n_prime, m_prime) / 2.0f);
        }

        /// <summary>
        /// Random variable with a gaussian distribution.
        /// </summary>
        Vector2 GaussianRandomVariable()
        {
            float x1, x2, w;
            do
            {
                x1 = (float)(2.0f * RandomHelpers.Random.NextDouble() - 1.0f);
                x2 = (float)(2.0f * RandomHelpers.Random.NextDouble() - 1.0f);
                w = x1 * x1 + x2 * x2;
            }
            while (w >= 1.0f);

            w = (float)Math.Sqrt((-2.0f * Math.Log(w)) / w);
            return new Vector2(x1 * w, x2 * w);
        }

        /// <summary>
        /// Gets the spectrum vaule for grid position n,m.
        /// </summary>
        float PhillipsSpectrum(int n_prime, int m_prime)
        {
            Vector2 k = new Vector2((float)(Math.PI * (2 * n_prime - N) / m_length), (float)(Math.PI * (2 * m_prime - N) / m_length));
            float k_length = k.Length;
            if (k_length < 0.000001f) return 0.0f;

            float k_length2 = k_length * k_length;
            float k_length4 = k_length2 * k_length2;

            k.Normalize();

            float k_dot_w = Vector2.Dot(k, m_windDirection);
            float k_dot_w2 = k_dot_w * k_dot_w * k_dot_w * k_dot_w * k_dot_w * k_dot_w;

            float w_length = m_windSpeed.Length;
            float L = w_length * w_length / GRAVITY;
            float L2 = L * L;

            float damping = 0.001f;
            float l2 = L2 * damping * damping;

            return (float)(m_waveAmp * Math.Exp(-1.0f / (k_length2 * L2)) / k_length4 * k_dot_w2 * Math.Exp(-k_length2 * l2));
        }

        float Dispersion(int n_prime, int m_prime)
        {
            double w_0 = 2.0f * Math.PI / 200.0f;
            double kx = Math.PI * (2 * n_prime - N) / m_length;
            double kz = Math.PI * (2 * m_prime - N) / m_length;
            return (float)(Math.Floor(Math.Sqrt(GRAVITY * Math.Sqrt(kx * kx + kz * kz)) / w_0) * w_0);
        }

        /// <summary>
        /// Inits the spectrum for time period t.
        /// </summary>
        Vector2 InitSpectrum(float t, int n_prime, int m_prime)
        {
            int index = m_prime * Nplus1 + n_prime;

            float omegat = m_dispersionTable[index] * t;

            float cos = (float)Math.Cos(omegat);
            float sin = (float)Math.Sin(omegat);

            float c0a = m_spectrum[index].X * cos - m_spectrum[index].Y * sin;
            float c0b = m_spectrum[index].X * sin + m_spectrum[index].Y * cos;

            float c1a = m_spectrum_conj[index].X * cos - m_spectrum_conj[index].Y * -sin;
            float c1b = m_spectrum_conj[index].X * -sin + m_spectrum_conj[index].Y * cos;

            return new Vector2(c0a + c1a, c0b + c1b);
        }


        /// <summary>
        /// Evaluates the waves for time period t. Must be thread safe.
        /// </summary>
        public void EvaluateWavesFFT(float t)
        {
            float kx, kz, len, lambda = -1.0f;
            int index, index1;

            for (int m_prime = 0; m_prime < N; m_prime++)
            {
                kz = (float)(Math.PI * (2.0f * m_prime - N) / m_length);

                for (int n_prime = 0; n_prime < N; n_prime++)
                {
                    kx = (float)(Math.PI * (2 * n_prime - N) / m_length);
                    len = (float)Math.Sqrt(kx * kx + kz * kz);
                    index = m_prime * N + n_prime;

                    Vector2 c = InitSpectrum(t, n_prime, m_prime);

                    m_heightBuffer[1, index].X = c.X;
                    m_heightBuffer[1, index].Y = c.Y;

                    m_slopeBuffer[1, index].X = -c.Y * kx;
                    m_slopeBuffer[1, index].Y = c.X * kx;

                    m_slopeBuffer[1, index].Z = -c.Y * kz;
                    m_slopeBuffer[1, index].W = c.X * kz;

                    if (len < 0.000001f)
                    {
                        m_displacementBuffer[1, index].X = 0.0f;
                        m_displacementBuffer[1, index].Y = 0.0f;
                        m_displacementBuffer[1, index].Z = 0.0f;
                        m_displacementBuffer[1, index].W = 0.0f;
                    }
                    else
                    {
                        m_displacementBuffer[1, index].X = -c.Y * -(kx / len);
                        m_displacementBuffer[1, index].Y = c.X * -(kx / len);
                        m_displacementBuffer[1, index].Z = -c.Y * -(kz / len);
                        m_displacementBuffer[1, index].W = c.X * -(kz / len);
                    }
                }
            }

            m_fourier.PeformFFT(0, m_heightBuffer, m_slopeBuffer, m_displacementBuffer);

            int sign;
            float[] signs = new float[] { 1.0f, -1.0f };
            Vector3 n;

            for (int m_prime = 0; m_prime < N; m_prime++)
            {
                for (int n_prime = 0; n_prime < N; n_prime++)
                {
                    index = m_prime * N + n_prime;          // index into buffers
                    index1 = m_prime * Nplus1 + n_prime;    // index into vertices

                    sign = (int)signs[(n_prime + m_prime) & 1];

                    // height
                    m_vertices[index1].Y = m_heightBuffer[1, index].X * sign;

                    // displacement
                    m_vertices[index1].X = m_position[index1].X + m_displacementBuffer[1, index].X * lambda * sign;
                    m_vertices[index1].Z = m_position[index1].Z + m_displacementBuffer[1, index].Z * lambda * sign;

                    // normal
                    n = new Vector3(-m_slopeBuffer[1, index].X * sign, 1.0f, -m_slopeBuffer[1, index].Z * sign);
                    n.Normalize();

                    m_normals[index1].X = n.X;
                    m_normals[index1].Y = n.Y;
                    m_normals[index1].Z = n.Z;


                    // for tiling
                    if (n_prime == 0 && m_prime == 0)
                    {
                        m_vertices[index1 + N + Nplus1 * N].Y = m_heightBuffer[1, index].X * sign;

                        m_vertices[index1 + N + Nplus1 * N].X = m_position[index1 + N + Nplus1 * N].X + m_displacementBuffer[1, index].X * lambda * sign;
                        m_vertices[index1 + N + Nplus1 * N].Z = m_position[index1 + N + Nplus1 * N].Z + m_displacementBuffer[1, index].Z * lambda * sign;

                        m_normals[index1 + N + Nplus1 * N].X = n.X;
                        m_normals[index1 + N + Nplus1 * N].Y = n.Y;
                        m_normals[index1 + N + Nplus1 * N].Z = n.Z;
                    }
                    if (n_prime == 0)
                    {
                        m_vertices[index1 + N].Y = m_heightBuffer[1, index].X * sign;

                        m_vertices[index1 + N].X = m_position[index1 + N].X + m_displacementBuffer[1, index].X * lambda * sign;
                        m_vertices[index1 + N].Z = m_position[index1 + N].Z + m_displacementBuffer[1, index].Z * lambda * sign;

                        m_normals[index1 + N].X = n.X;
                        m_normals[index1 + N].Y = n.Y;
                        m_normals[index1 + N].Z = n.Z;
                    }
                    if (m_prime == 0)
                    {
                        m_vertices[index1 + Nplus1 * N].Y = m_heightBuffer[1, index].X * sign;

                        m_vertices[index1 + Nplus1 * N].X = m_position[index1 + Nplus1 * N].X + m_displacementBuffer[1, index].X * lambda * sign;
                        m_vertices[index1 + Nplus1 * N].Z = m_position[index1 + Nplus1 * N].Z + m_displacementBuffer[1, index].Z * lambda * sign;

                        m_normals[index1 + Nplus1 * N].X = n.X;
                        m_normals[index1 + Nplus1 * N].Y = n.Y;
                        m_normals[index1 + Nplus1 * N].Z = n.Z;
                    }
                }
            }



        }

    }
}
