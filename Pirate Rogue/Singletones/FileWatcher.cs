﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.IO;
using NLog;

namespace Pirate_Rogue.Singletones
{
    public static class FileWatcher
    {
        private static ILogger logger;
        private static bool needToReset;
        private static bool initialized;
        private static string defaultResPath;
        private static Dictionary<string,DateTime> lastChangesDictionary; 
        public static bool NeedToReset
        {
            private set { needToReset = value; }
            get
            {
                bool v = needToReset;
                needToReset = false;
                return v;
            }
        }

        public static void Initialize(string relativePathToFilesOnDisk)
        {
/*            if (!Globals.ChangableValues.PerformanceSettings.DebugMode)
                return;*/
            if (logger == null)
            {
                logger = LogManager.GetCurrentClassLogger();
            }
            initialized = true;
            defaultResPath = Globals.FilePaths.RES_PATH;
            Globals.FilePaths.RES_PATH = relativePathToFilesOnDisk;
            lastChangesDictionary = new Dictionary<string, DateTime>();
        }
        public static void Update()
        {
            if (!initialized)
                return;
            //wait till program responds to that
            if (needToReset)
                return;
            //TODO: check *every* file in res folder?
            string folderPath = Globals.FilePaths.GetImagesDirPath();
            if (!Directory.Exists(folderPath))
            {
                logger.Error("There is no image folder in path: "+folderPath);
                return;
            }
            string[] filesToCheckArray=Directory.GetFiles(folderPath);
            foreach (string filePath in filesToCheckArray)
            {
                DateTime currentTime = File.GetLastWriteTime(filePath);
                if (lastChangesDictionary.ContainsKey(filePath))
                {
                    DateTime lastTime = lastChangesDictionary[filePath];
                    if (lastTime != currentTime)
                    {
                        NeedToReset = true;
                        lastChangesDictionary[filePath]=currentTime;
                        return;
                    }
                    continue;
                }
                lastChangesDictionary.Add(filePath,currentTime);
            }
        }
    }
}
