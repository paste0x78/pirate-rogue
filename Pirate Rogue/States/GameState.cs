﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/


using System;
using Artemis;
using Artemis.Manager;
using Artemis.System;
using NLog;
using OpenTK;
using Pirate_Rogue.Components;
using Pirate_Rogue.Singletones;
using Pirate_Rogue.Systems;
using Pirate_Rogue.Systems.Input;
using Pirate_Rogue.Templates;
using RLNET;

namespace Pirate_Rogue
{
    internal class GameState
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();


        private EntityWorld entityWorld;
        private Camera camera;
        private Ocean ocean;

        public bool Initialized { private set; get; }
    

        public GameState(RLRootConsole rootConsole)
        {
            Initialized = false;
            ControlsConfigLoader.LoadConfigFile();
            AudioPlayer.SetVolume(1f);
            StartArtemisFramework(rootConsole);
        }

        public void Reset(RLRootConsole rootConsole)
        {
            StartArtemisFramework(rootConsole);
        }

        private void StartArtemisFramework(RLRootConsole rootConsole)
        {
            Initialized = false;

            //TODO: create new entities leave systems alone!
            DisposeArtemisIfExists();

            Globals.Bounds playerBounds = new Globals.Bounds();
            playerBounds.min = new Vector2();
            playerBounds.max = new Vector2();
            camera = new Camera(0, 0, playerBounds);
            ocean = new Ocean();

            entityWorld = new EntityWorld();

            entityWorld.SetPool(typeof (CWindRaycast),
                new ComponentPool<ComponentPoolable>(100, 20, true, type => new CWindRaycast(), typeof (CWindRaycast)));
            entityWorld.SetPool(typeof (CTransform),
                new ComponentPool<ComponentPoolable>(200, 40, true, type => new CTransform(), typeof (CTransform)));

            int layer = 0;
            entityWorld.SystemManager.SetSystem(new SOceanSoundsPlayer(), GameLoopType.Update, layer);
            entityWorld.SystemManager.SetSystem(new SSunEvaluator(), GameLoopType.Update, layer);
            layer++;
            entityWorld.SystemManager.SetSystem(new SPlayerInput(Globals.EnitityTags.Player.ToString()),
                GameLoopType.Update, layer);
            entityWorld.SystemManager.SetSystem(new SCameraInput(Globals.EnitityTags.Player.ToString()),
                GameLoopType.Update, layer);
            entityWorld.SystemManager.SetSystem(new SDebugInput(Globals.EnitityTags.Player.ToString()),
                GameLoopType.Update, layer);
            entityWorld.SystemManager.SetSystem(new SSunInput(Globals.EnitityTags.SunSettings.ToString()), 
      GameLoopType.Update, layer);
            //entityWorld.SystemManager.SetSystem(new SWindInput(Globals.EnitityTags.WindSettings.ToString()),
            //    GameLoopType.Update, layer);
            //entityWorld.SystemManager.SetSystem(new SOceanAnimatorSeascape(), GameLoopType.Draw, layer);
            //entityWorld.SystemManager.SetSystem(new SOceanAnimatorWaterworld(), GameLoopType.Update, layer);
            layer++;
            entityWorld.SystemManager.SetSystem(new SCameraPlayerFollower(Globals.EnitityTags.Player.ToString()),
                GameLoopType.Update, layer);
            entityWorld.SystemManager.SetSystem(new SModelMover(), GameLoopType.Update, layer);
            //entityWorld.SystemManager.SetSystem(new SWindCreator(new TimeSpan(0,0,0,0,200), Globals.EnitityTags.WindSettings.ToString()), GameLoopType.Update, layer);
            layer++;
            entityWorld.SystemManager.SetSystem(new SModelRotator(), GameLoopType.Update, layer);
            layer++;
            entityWorld.SystemManager.SetSystem(new SWindBroadCollisionChecker(), GameLoopType.Update, layer);
            entityWorld.SystemManager.SetSystem(new SCollisionDetector(), GameLoopType.Update, layer);
            layer++;
            entityWorld.SystemManager.SetSystem(new SWaterResistanceApplier(), GameLoopType.Update, layer);
            layer++;    
            entityWorld.SystemManager.SetSystem(new SVelocityApplier(), GameLoopType.Update, layer);
            layer++;
            entityWorld.SystemManager.SetSystem(new SFPSCalculator(), GameLoopType.Update, layer);

            layer = 0;
            entityWorld.SystemManager.SetSystem(new SOceanStaticRenderer(), GameLoopType.Draw, layer);
            layer++;
            //entityWorld.SystemManager.SetSystem(new SOceanTileAnimator(), GameLoopType.Draw, layer);
            layer++;
            entityWorld.SystemManager.SetSystem(new SScreenBufferDrawer(), GameLoopType.Draw, layer);
            layer++;
            //if you want to draw dynamic object (f.e. rotated object),not by passing cells to screen buffer, set drawing systems from here
            entityWorld.SystemManager.SetSystem(new SModelRenderer(), GameLoopType.Draw, layer);
            entityWorld.SystemManager.SetSystem(new SModelAnimator(), GameLoopType.Draw, layer);
            entityWorld.SystemManager.SetSystem(new SSailsAnimator(), GameLoopType.Draw, layer);
            layer++;
            entityWorld.SystemManager.SetSystem(new SDebugGUIArtemisInfoRenderer(), GameLoopType.Draw, layer);
            entityWorld.SystemManager.SetSystem(new SDebugGUIFPSInfoRenderer(), GameLoopType.Draw, layer);
            entityWorld.SystemManager.SetSystem(new SDebugGUIPlayerInfoRenderer(), GameLoopType.Draw, layer);
            entityWorld.SystemManager.SetSystem(new SDebugGUIWindRenderer(Globals.EnitityTags.WindSettings.ToString()),
                GameLoopType.Draw, layer);

            //EntityCreator.CreateTerrain(entityWorld, rootConsole, Vector2.Zero);
            EntityCreator.CreateShip(entityWorld, rootConsole);
            EntityCreator.CreatePlayer(entityWorld, rootConsole, Globals.EnitityTags.Player.ToString());
            EntityCreator.CreateWindSettings(entityWorld, rootConsole, Globals.EnitityTags.WindSettings.ToString());
            EntityCreator.CreateFPSCounter(entityWorld);
            EntityCreator.CreateWaterSounds(entityWorld,rootConsole,Globals.FilePaths.GetWaterSoundsPath());
            EntityCreator.CreateSunSettings(entityWorld,rootConsole,Globals.EnitityTags.SunSettings.ToString());

            EntitySystem.BlackBoard.SetEntry<RLRootConsole>(typeof (RLRootConsole).ToString(), rootConsole);
            EntitySystem.BlackBoard.SetEntry<Camera>(typeof (Camera).ToString(), camera);
            ocean.Start();
            EntitySystem.BlackBoard.SetEntry<Ocean>(typeof (Ocean).ToString(), ocean);

            Initialized = true;
            logger.Debug("Initialized Artemis system");
        }

        public void DisposeArtemisIfExists()
        {
            if (entityWorld != null)
            {
                foreach (var system in entityWorld.SystemManager.Systems)
                {
                    system.IsEnabled = false;
                }
                entityWorld.SystemManager.Systems.Clear();

                entityWorld.Clear();
                entityWorld = null;
            }
            camera = null;
            Initialized = false;

        }


        public Globals.GameState OnRootConsoleUpdate(RLRootConsole rootConsole, Globals.GameState currentGameState, double time)
        {
            
            entityWorld.Update((long) (time*Globals.ReadOnlyValues.TicksInSeconds));
            AudioPlayer.Update(time);
            foreach (RLKeyEvent key in rootConsole.Keyboard.PollKeyEvents())
            {
                switch (key.Key)
                {
                    case RLKey.Escape:
                        currentGameState = Globals.GameState.PauseMenu;
                        break;
                }
            }
            return currentGameState;
            
        }


        public void OnRootConsoleRender(RLRootConsole rootConsole, Globals.GameState currentGameState)
        {
            rootConsole.Clear();
            //handled by system
            //rootConsole.ClearGameScreenBuffer();
            rootConsole.StartDrawing(camera.ZoomOut);

            entityWorld.Draw();
            rootConsole.DrawGUI(Globals.ReadOnlyValues.TransparentColor);
            rootConsole.EndDrawing();
        }

    }
}
