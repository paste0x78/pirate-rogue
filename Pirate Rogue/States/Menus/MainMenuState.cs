﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.IO;
using Pirate_Rogue.Singletones;
using Pirate_Rogue.States.Menus;
using RLNET;

namespace Pirate_Rogue
{
    enum MainMenuSelection
    {
        NewGameContinue,
        Options,
        Credits,
        Exit,
    }
    //TODO: interface for gamestates?
    class MainMenuState : AbstractMenuState
    {

        private double acumulatedDeltaIntro;
        private double acumulatedDeltaLogo;
        public MainMenuState()
        {
            menuBackgroundLayers = new RLDynamicObject[4];
            for (int i = 0; i < 4; i++)
            {
                RexReaderHelper.LoadXP(Globals.FilePaths.GetLogoBackgroundPath(), false, i,
                    ref menuBackgroundLayers[i]);        
            }
            selection = MainMenuSelection.NewGameContinue;
            selected = selection;
            acumulatedDeltaIntro = 0;
            acumulatedDeltaLogo = 0;
            childrenMenuStates = new List<AbstractMenuState>();
            childrenMenuStates.Add(new OptionState(menuBackgroundLayers,this));
            childrenMenuStates.Add(new CreditsState(menuBackgroundLayers, this));
        }


        public override Enum GetEnumSelection()
        {
            return MainMenuSelection.Credits;
        }

        public override Globals.GameState ParseSelection(RLRootConsole rootConsole, Enum currentSelection, Globals.GameState selectedGameState)
        {
/*            if (acumulatedDeltaLogo < 70)
            {
                acumulatedDeltaLogo = 100;
                return selectedGameState;
            }*/
            if (acumulatedDeltaIntro < 70)
            {
                acumulatedDeltaIntro = 100;
                return selectedGameState;
            }
            MainMenuSelection mainMenuSelection = (MainMenuSelection)currentSelection;
            switch (mainMenuSelection)
            {
                case MainMenuSelection.NewGameContinue:
                    selectedGameState = Globals.GameState.InTheGame;
                    break;
                case MainMenuSelection.Options:
                case MainMenuSelection.Credits:
                    selected = currentSelection;
                    break;
                case MainMenuSelection.Exit:
                    rootConsole.Close();
                    break;
            }
            return selectedGameState;
        }

        public override Globals.GameState OnRootConsoleUpdate(RLRootConsole rootConsole, Globals.GameState currentGameState)
        {
            if (currentGameState == Globals.GameState.InTheGame)
                return currentGameState;

            MainMenuSelection mainMenuSelection = (MainMenuSelection)selected;
            if (childrenMenuStates.Count != 0)
            {
                switch (mainMenuSelection)
                {
                    case MainMenuSelection.Options:
                        return childrenMenuStates[0].OnRootConsoleUpdate(rootConsole, currentGameState);
                    case MainMenuSelection.Credits:
                        return childrenMenuStates[1].OnRootConsoleUpdate(rootConsole, currentGameState);

                }
            }
            currentGameState = base.OnRootConsoleUpdate(rootConsole, currentGameState);


            return currentGameState;
        }

        private bool RenderLogo(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta)
        {
            if (acumulatedDeltaLogo >= 70)
                return false;
            RLColor background = RLColor.Black;
            RLColor foregroundNotSelected = RLColor.White;
            RLColor foregroundSelected = RLColor.LightRed;
            int width = rootConsole.Width / 2 - 6;
            int height = rootConsole.Height / 2 - 10;
            string horBar = "Press Enter to continue...";
            //rootConsole.Print(width, height - 1, horBar, foregroundNotSelected, background);
            foreach (string line in File.ReadAllLines(Globals.FilePaths.GetLogoFilePath()))
            {

                rootConsole.Print(width - line.Length / 2 + 5, height, line, (foregroundNotSelected),
                    background);
                height++;
            }
            return true;
        }
        private bool RenderMenuAnimation(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta)
        {
            if (acumulatedDeltaIntro >= 70)
                return false;
            int n = 0;
            foreach (RLDynamicObject layer in menuBackgroundLayers)
            {
                for (int x = 0; x < layer.renderedCells.GetLength(0); x++)
                {
                    for (int y = 0; y < layer.renderedCells.GetLength(1); y++)
                    {
                        if (layer.renderedCells[x, y].character > 1)
                        {
                            if (n == 3)
                            {
                                if (y == 15)
                                {
                                    string s = "Press start to skip intro";
                                    rootConsole.Print(rootConsole.Width/2 - s.Length/2, (int) (y + acumulatedDeltaIntro), s,
                                        RLColor.Black, RLColor.White);
                                }
                                {
                                    rootConsole.Set(x + 2, (int) (y + acumulatedDeltaIntro), layer.renderedCells[x, y]);
                                }
                            }
                            else
                                rootConsole.Set(x + 2, y, layer.renderedCells[x, y]);
                        }
                    }
                }
                n++;
            }
            acumulatedDeltaIntro += delta*25;
            return true;
        }
        public override void OnRootConsoleRender(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta)
        {
            if (currentGameState == Globals.GameState.InTheGame)
                return;

            MainMenuSelection mainMenuSelection = (MainMenuSelection) selected;
            if (childrenMenuStates.Count != 0)
            {
                switch (mainMenuSelection)
                {
                    case MainMenuSelection.Options:
                        childrenMenuStates[0].OnRootConsoleRender(rootConsole, currentGameState, delta);
                        return;
                    case MainMenuSelection.Credits:
                        childrenMenuStates[1].OnRootConsoleRender(rootConsole, currentGameState, delta);
                        return;
                }
            }
            base.OnRootConsoleRender(rootConsole, currentGameState, delta);
        }

        protected override void DrawGUIElements(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta)
        {
/*            if (RenderLogo(rootConsole, currentGameState, delta))
                return;*/
            if (RenderMenuAnimation(rootConsole, currentGameState, delta))
                return;
            //TODO: move those to config file
            RLColor background = RLColor.White;
            RLColor foregroundNotSelected = RLColor.Black;
            RLColor foregroundSelected = RLColor.Red;
            int width = rootConsole.Width/2 - 10;
            int height = rootConsole.Height/2;
            DrawMenuLabels(rootConsole,background,foregroundNotSelected,foregroundSelected,width,height);
        }
    }
}
