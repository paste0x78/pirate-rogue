﻿
/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using RLNET;
using System;
using System.Collections.Generic;

namespace Pirate_Rogue.States.Menus
{
    abstract class AbstractMenuState
    {
        protected AbstractMenuState parentMenuState;
        protected List<AbstractMenuState> childrenMenuStates;
 
        protected Enum selection;
        protected internal Enum selected;
        protected RLDynamicObject[] menuBackgroundLayers;

        public abstract Enum GetEnumSelection();
        public abstract Globals.GameState ParseSelection(RLRootConsole rootConsole, Enum currentSelection, Globals.GameState selectedGameState);

        protected abstract void DrawGUIElements(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta);

        protected void DrawMenuLabels(RLGUIConsole rootConsole,RLColor background, RLColor foregroundNotSelected, RLColor foregroundSelected, int width, int height)
        {
            string horBar = "~~~~~~~~~~";
            rootConsole.Print(width, height - 1, horBar, foregroundNotSelected, background);
            foreach (var name in Enum.GetValues(GetEnumSelection().GetType()))
            {

                rootConsole.Print(width, height,
                    ((name.ToString()).Equals(selection.ToString()) ? " * " : " ") + name.ToString(),
                    ((name.ToString()).Equals(selection.ToString()) ? foregroundSelected : foregroundNotSelected),
                    background);
                height++;
            }
            rootConsole.Print(width, height, horBar.Replace(horBar[0], ' '), foregroundNotSelected, background);
            rootConsole.Print(width, height + 1, horBar, foregroundNotSelected, background);
            //footer
            rootConsole.Print(rootConsole.Width - 10, rootConsole.Height - 2,
                "b02",foregroundNotSelected, background);
            string copyright = "Copyright 2016, Maciej 'DagonDev' Szewczyk";
            string link = "https://www.reddit.com/r/piraterogue/";
            rootConsole.Print(rootConsole.Width/2 -copyright.Length/2-2, rootConsole.Height - 4,copyright, foregroundNotSelected, background);
            rootConsole.Print(rootConsole.Width / 2 - link.Length / 2-2, rootConsole.Height-3, link, foregroundSelected, background);
        }
        public virtual Globals.GameState OnRootConsoleUpdate(RLRootConsole rootConsole, Globals.GameState currentGameState)
        {
            if (currentGameState == Globals.GameState.InTheGame)
                return currentGameState;
            Array selections;
            int index;
            foreach (RLKeyEvent key in rootConsole.Keyboard.PollKeyEvents())
            {
                if (!key.Pressed)
                {
                    switch (key.Key)
                    {
                        case RLKey.Up:
                            index = 0;
                            selections = Enum.GetValues(GetEnumSelection().GetType());//OptionsSelection));
                            foreach (var name in selections)
                            {
                                if ((name.ToString()).Equals(selection.ToString()))
                                    break;
                                index++;
                            }

                            if (index == 0)
                                index = selections.Length - 1;
                            else
                            {
                                index--;
                            }

                            index %= selections.Length;
                            selection = (Enum) selections.GetValue(index);
                            break;
                        case RLKey.Down:
                            index = 0;
                            selections = Enum.GetValues(GetEnumSelection().GetType());
                            foreach (var name in selections)
                            {
                                if ((name.ToString()).Equals(selection.ToString()))
                                    break;
                                index++;
                            }
                            index++;
                            index %= selections.Length;
                            selection = (Enum)selections.GetValue(index);
                            break;
                        case RLKey.Enter:
                            currentGameState=ParseSelection(rootConsole,selection,currentGameState);
                            break;
                    }
                }
            }
            return currentGameState;

        }
        public virtual void OnRootConsoleRender(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta)
        {
            if (currentGameState == Globals.GameState.InTheGame)
                return;
            rootConsole.Clear();
            rootConsole.StartDrawing(1);
            int n = 0;
            foreach (RLDynamicObject layer in menuBackgroundLayers)
            {
                if (n == 3)
                    continue;
                for (int x = 0; x < layer.renderedCells.GetLength(0); x++)
                {
                    for (int y = 0; y < layer.renderedCells.GetLength(1); y++)
                    {
                        if (layer.renderedCells[x, y].character > 1)
                            rootConsole.Set(x + 2, y, layer.renderedCells[x, y]);
                    }
                }

                n++;
            }

            DrawGUIElements(rootConsole, currentGameState, delta);
            rootConsole.DrawGUI(Globals.ReadOnlyValues.TransparentColor);
            rootConsole.EndDrawing();
        }
    }
}
