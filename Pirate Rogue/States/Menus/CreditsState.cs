﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.IO;
using Pirate_Rogue.States.Menus.Options;
using RLNET;

namespace Pirate_Rogue.States.Menus
{
    enum CreditsSelection
    {
        Exit
    }
    class CreditsState : AbstractMenuState
    {
        public CreditsState(RLDynamicObject[] menuBackgroundLayers, AbstractMenuState parentMenuState)
        {
            this.menuBackgroundLayers = menuBackgroundLayers;
            selection = OptionsSelection.Controls;
            selected = selection;
            this.parentMenuState = parentMenuState;
            this.childrenMenuStates = new List<AbstractMenuState>();
        }

        public override Enum GetEnumSelection()
        {
            return CreditsSelection.Exit;
        }

        public override Globals.GameState ParseSelection(RLRootConsole rootConsole, Enum currentSelection, Globals.GameState selectedGameState)
        {
            CreditsSelection sel = (CreditsSelection)currentSelection;
            switch (sel)
            {
                case CreditsSelection.Exit:
                    parentMenuState.selected = MainMenuSelection.NewGameContinue;
                    break;
            }
            return selectedGameState;
        }

        protected override void DrawGUIElements(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta)
        {
            //TODO: move those to config file
            RLColor background = RLColor.White;
            RLColor foregroundNotSelected = RLColor.Black;
            RLColor foregroundSelected = RLColor.LightRed;
            int width = rootConsole.Width / 2 - 6;
            int height = rootConsole.Height / 2-10;
            string horBar = "Press Enter to continue...";
            //rootConsole.Print(width, height - 1, horBar, foregroundNotSelected, background);
            foreach (string line in File.ReadAllLines(Globals.FilePaths.GetCreditsFilePath()))
            {

                rootConsole.Print(width-line.Length/2+5, height, line, (height%2==0?foregroundNotSelected:foregroundSelected),
                    background);
                height++;
            }
            //rootConsole.Print(width, height, horBar.Replace(horBar[0], ' '), foregroundNotSelected, background);
            rootConsole.Print(width- horBar.Length/2+5, height + 3, horBar, foregroundNotSelected, background);
            //footer
            rootConsole.Print(rootConsole.Width - 10, rootConsole.Height - 2,
                "v0.01", foregroundSelected, background);
        }
    }
}
