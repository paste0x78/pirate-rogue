﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using Pirate_Rogue.States.Menus;
using Pirate_Rogue.States.Menus.Options;
using RLNET;

namespace Pirate_Rogue
{
    enum OptionsSelection
    {
        //Video,
        //Audio,
        Controls,
        Exit
    }
    //TODO: interface for gamestates?
    class OptionState : AbstractMenuState
    {

        public OptionState(RLDynamicObject[] menuBackgroundLayers, AbstractMenuState parentMenuState)
        {
            this.menuBackgroundLayers = menuBackgroundLayers;
            selection = OptionsSelection.Exit;
            selected = selection;
            this.parentMenuState = parentMenuState;
            this.childrenMenuStates = new List<AbstractMenuState>();
            childrenMenuStates.Add(new ControlsState(menuBackgroundLayers,this));
        }


        protected override void DrawGUIElements(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta)
        {
            //TODO: move those to config file
            RLColor background = RLColor.White;
            RLColor foregroundNotSelected = RLColor.Black;
            RLColor foregroundSelected = RLColor.Red;
            int width = rootConsole.Width/2 - 10;
            int height = rootConsole.Height/2;
            DrawMenuLabels(rootConsole,background,foregroundNotSelected,foregroundSelected,width,height);

        }

        public override Enum GetEnumSelection()
        {
            return OptionsSelection.Controls;
        }
        public override Globals.GameState OnRootConsoleUpdate(RLRootConsole rootConsole, Globals.GameState currentGameState)
        {
            if (currentGameState == Globals.GameState.InTheGame)
                return currentGameState;
            OptionsSelection selection = (OptionsSelection)selected;
            if (childrenMenuStates.Count != 0)
            {
                switch (selection)
                {
                    case OptionsSelection.Controls:
                        return childrenMenuStates[0].OnRootConsoleUpdate(rootConsole, currentGameState);

                }
            }
            currentGameState = base.OnRootConsoleUpdate(rootConsole, currentGameState);


            return currentGameState;
        }
        public override void OnRootConsoleRender(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta)
        {
            if (currentGameState == Globals.GameState.InTheGame)
                return;

            MainMenuSelection mainMenuSelection = (MainMenuSelection)selected;
            if (childrenMenuStates.Count != 0)
            {
                OptionsSelection selection = (OptionsSelection) selected;
                switch (selection)
                {
                    case OptionsSelection.Controls:
                         childrenMenuStates[0].OnRootConsoleRender(rootConsole, currentGameState,delta);
                        return;

                }
            }
            base.OnRootConsoleRender(rootConsole, currentGameState, delta);
        }
        public override Globals.GameState ParseSelection(RLRootConsole rootConsole, Enum currentSelection, Globals.GameState selectedGameState)
        {
            OptionsSelection sel = (OptionsSelection) currentSelection;
            switch (sel)
            {
                case OptionsSelection.Exit:
                    parentMenuState.selected = MainMenuSelection.NewGameContinue;
                    break;
                case OptionsSelection.Controls:
                    this.selected = sel;
                    break;
            }
            return selectedGameState;
        }
    }
}
