﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using Pirate_Rogue.Globals;
using Pirate_Rogue.Singletones;
using RLNET;

namespace Pirate_Rogue.States.Menus.Options
{
    class ControlsState : AbstractMenuState
    {
        private bool selectingKey;
        enum ControlsSelection
        {
            RotateShipLeft,
            RotateShipRight,
            RotateSailsLeft,
            RotateSailsRight,
            SetSailsUp,
            SetSailsDown,
            ZoomIn,
            ZoomOut,
            Exit,
        }
        public ControlsState(RLDynamicObject[] menuBackgroundLayers, AbstractMenuState parentMenuState)
        {
            this.menuBackgroundLayers = menuBackgroundLayers;
            selection = ControlsSelection.RotateSailsLeft;
            selected = selection;
            this.parentMenuState = parentMenuState;
        }

        public override Globals.GameState OnRootConsoleUpdate(RLRootConsole rootConsole, Globals.GameState currentGameState)
        {
            if (!selectingKey) 
                return base.OnRootConsoleUpdate(rootConsole, currentGameState);
            foreach (RLKeyEvent key in rootConsole.Keyboard.PollKeyEvents())
            {
                if (!key.Pressed)
                {
                    selectingKey = false;
                    switch (key.Key)
                    {
                        case RLKey.Enter:
                        case RLKey.Escape:
                            return currentGameState;

                    }
                    ControlsSelection sel = (ControlsSelection) selected;
                    switch (sel)
                    {
                        case ControlsSelection.Exit:                           
                            break;
                        case ControlsSelection.RotateSailsLeft:
                            ChangableValues.InputSettings.RotateSailsLeft = key.Key;
                            SaveLoadManager.SaveStringToConfigFile(Globals.FilePaths.GetControlsConfigFilePath(),Globals.FilePaths.SAVE_PATH,Globals.ConfigFileEnum.RotateSailsLeft, key.Key.ToString());
                            break;
                        case ControlsSelection.RotateSailsRight:
                            ChangableValues.InputSettings.RotateSailsRight = key.Key;
                            SaveLoadManager.SaveStringToConfigFile(Globals.FilePaths.GetControlsConfigFilePath(), Globals.FilePaths.SAVE_PATH, Globals.ConfigFileEnum.RotateSailsRight, key.Key.ToString());
                            break;
                        case ControlsSelection.RotateShipLeft:
                            ChangableValues.InputSettings.RotateShipLeft = key.Key;
                            SaveLoadManager.SaveStringToConfigFile(Globals.FilePaths.GetControlsConfigFilePath(), Globals.FilePaths.SAVE_PATH, Globals.ConfigFileEnum.RotateShipLeft, key.Key.ToString());
                            break;
                        case ControlsSelection.RotateShipRight:
                            ChangableValues.InputSettings.RotateShipRight = key.Key;
                            SaveLoadManager.SaveStringToConfigFile(Globals.FilePaths.GetControlsConfigFilePath(), Globals.FilePaths.SAVE_PATH, Globals.ConfigFileEnum.RotateShipRight, key.Key.ToString());
                            break;
                        case ControlsSelection.SetSailsDown:
                            ChangableValues.InputSettings.SetSailsDown = key.Key;
                            SaveLoadManager.SaveStringToConfigFile(Globals.FilePaths.GetControlsConfigFilePath(), Globals.FilePaths.SAVE_PATH, Globals.ConfigFileEnum.SetSailsDown, key.Key.ToString());
                            break;
                        case ControlsSelection.SetSailsUp:
                            ChangableValues.InputSettings.SetSailsUp = key.Key;
                            SaveLoadManager.SaveStringToConfigFile(Globals.FilePaths.GetControlsConfigFilePath(), Globals.FilePaths.SAVE_PATH, Globals.ConfigFileEnum.SetSailsUp, key.Key.ToString());
                            break;
                        case ControlsSelection.ZoomIn:
                            ChangableValues.InputSettings.ZoomIn = key.Key;
                            SaveLoadManager.SaveStringToConfigFile(Globals.FilePaths.GetControlsConfigFilePath(), Globals.FilePaths.SAVE_PATH, Globals.ConfigFileEnum.ZoomIn, key.Key.ToString());
                            break;
                        case ControlsSelection.ZoomOut:
                            ChangableValues.InputSettings.ZoomOut = key.Key;
                            SaveLoadManager.SaveStringToConfigFile(Globals.FilePaths.GetControlsConfigFilePath(), Globals.FilePaths.SAVE_PATH, Globals.ConfigFileEnum.ZoomOut, key.Key.ToString());
                            break;
                    }
                }
            }
            return currentGameState;
        }

        protected override void DrawGUIElements(RLRootConsole rootConsole, Globals.GameState currentGameState, double delta)
        {
            //TODO: move those to config file
            RLColor background = RLColor.White;
            RLColor foregroundNotSelected = RLColor.Black;
            RLColor foregroundSelected = RLColor.Red;
            int width = rootConsole.Width / 2 - 10;
            int height = rootConsole.Height / 2;

            string horBar = "~~~~~~~~~~";
            rootConsole.Print(width, height - 1, horBar, foregroundNotSelected, background);
            string keyInput = "";
            
            foreach (var name in Enum.GetValues(GetEnumSelection().GetType()))
            {
                ControlsSelection select = (ControlsSelection) name;
                switch (select)
                {
                    case ControlsSelection.RotateSailsLeft:
                        keyInput = ChangableValues.InputSettings.RotateSailsLeft.ToString();
                        break;
                    case ControlsSelection.RotateSailsRight:
                        keyInput = ChangableValues.InputSettings.RotateSailsRight.ToString();
                        break;
                    case ControlsSelection.RotateShipLeft:
                        keyInput = ChangableValues.InputSettings.RotateShipLeft.ToString();
                        break;
                    case ControlsSelection.RotateShipRight:
                        keyInput = ChangableValues.InputSettings.RotateShipRight.ToString();
                        break;
                    case ControlsSelection.SetSailsUp:
                        keyInput = ChangableValues.InputSettings.SetSailsUp.ToString();
                        break;
                    case ControlsSelection.SetSailsDown:
                        keyInput = ChangableValues.InputSettings.SetSailsDown.ToString();
                        break;
                    case ControlsSelection.ZoomIn:
                        keyInput = ChangableValues.InputSettings.ZoomIn.ToString();
                        break;
                    case ControlsSelection.ZoomOut:
                        keyInput = ChangableValues.InputSettings.ZoomOut.ToString();
                        break;
                    case ControlsSelection.Exit:
                        keyInput = "";
                        break;
                }
                rootConsole.Print(width, height,
                    ((name.ToString()).Equals(selection.ToString()) ? " * " : " ") + name.ToString()+" "+(selectingKey&& (name.ToString()).Equals(selection.ToString()) ? "[":"(")+ keyInput + (selectingKey&&(name.ToString()).Equals(selection.ToString()) ? "]" : ")"),
                    ((name.ToString()).Equals(selection.ToString()) ? foregroundSelected : foregroundNotSelected),
                    background);
                height++;
            }
            rootConsole.Print(width, height, horBar.Replace(horBar[0], ' '), foregroundNotSelected, background);
            rootConsole.Print(width, height + 1, horBar, foregroundNotSelected, background);
            //footer
            rootConsole.Print(rootConsole.Width - 10, rootConsole.Height - 2,
                "v0.01", foregroundSelected, background);
        }

        public override Enum GetEnumSelection()
        {
            return ControlsSelection.RotateSailsLeft;
        }

        public override Globals.GameState ParseSelection(RLRootConsole rootConsole, Enum currentSelection, Globals.GameState selectedGameState)
        {
            ControlsSelection sel = (ControlsSelection)currentSelection;
            this.selected = sel;
            switch (sel)
            {
                case ControlsSelection.Exit:
                    parentMenuState.selected = OptionsSelection.Exit;
                    break;
                default:              
                    selectingKey = !selectingKey;
                    break;
            }
            return selectedGameState;
        }
    }
}
