﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/


using System;
using RLNET;

namespace Pirate_Rogue.Globals
{

    public static class ReadOnlyValues
    {
        public static readonly long TicksInSeconds = TimeSpan.TicksPerSecond;
        public static readonly float SecondsInTicks = 1f / TicksInSeconds;
        //TODO: use MathHelper
        public static readonly double RadiansInDegrees = (Math.PI / 180.000);
        public static readonly double DegreesInRadian = (180.000 / Math.PI);
        public static readonly RLColor TransparentColor = new RLColor(1f, 0f, 1f);
        public static readonly RLColor TransparentColor2 = new RLColor(0.8f, 0f, 1f);
        public static readonly RLColor[] WaterColors = new RLColor[]
            {
                new RLColor(0f,32f/255f,64/255f),
                new RLColor(0f,51f/255f,102f/255f),
                new RLColor(0f,70f/255f,140f/255f),
                new RLColor(0f,89f/255f,178f/255f),
                new RLColor(0f,108f/255f,217f/255f),
                new RLColor(0f,136f/255f,235f/255f),
                new RLColor(102f,178f/255f,255f/255f),
            };
    }

    public static class ChangableValues
    {
        public static class InputSettings
        {
            public static RLKey RotateShipLeft = RLKey.A;
            public static RLKey RotateShipRight = RLKey.D;
            public static RLKey RotateSailsLeft = RLKey.Q;
            public static RLKey RotateSailsRight = RLKey.E;
            public static RLKey SetSailsUp = RLKey.W;
            public static RLKey SetSailsDown = RLKey.S;
            public static RLKey ZoomIn = RLKey.R;
            public static RLKey ZoomOut = RLKey.F;
            public static RLKey DebugMode = RLKey.F1;
        }

        public static class PerformanceSettings
        {
            public static float MaximumZoomOut = 2f;
            public static bool DebugMode = false;
        }
    }
}
