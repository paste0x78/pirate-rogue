﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using OpenTK;

namespace Pirate_Rogue.Globals
{
    //https://github.com/SebLague/2DPlatformer-Tutorial/blob/master/Episode 11/CameraFollow.cs
    public struct Bounds
    {
        public Vector2 center
        {
            get
            {
                return min + size / 2;
            }
        }
        public Vector2 min;
        public Vector2 max;

        public Vector2 size
        {
            get { return max - min; }
        }
    }
    public struct FocusArea
    {
        public Vector2 centre;
        public Vector2 velocity;
        float left, right;
        float top, bottom;


        public FocusArea(Bounds targetBounds, Vector2 size)
        {
            left = targetBounds.center.X - size.X / 2;
            right = targetBounds.center.X + size.X / 2;
            bottom = targetBounds.min.Y;
            top = targetBounds.min.Y + size.Y;

            velocity = Vector2.Zero;
            centre = new Vector2((left + right) / 2, (top + bottom) / 2);
        }

        public void Update(Bounds targetBounds)
        {
            float shiftX = 0;
            if (targetBounds.min.X < left)
            {
                shiftX = targetBounds.min.X - left;
            }
            else if (targetBounds.max.X > right)
            {
                shiftX = targetBounds.max.X - right;
            }
            left += shiftX;
            right += shiftX;

            float shiftY = 0;
            if (targetBounds.min.Y < bottom)
            {
                shiftY = targetBounds.min.Y - bottom;
            }
            else if (targetBounds.max.Y > top)
            {
                shiftY = targetBounds.max.Y - top;
            }
            top += shiftY;
            bottom += shiftY;
            centre = new Vector2((left + right) / 2, (top + bottom) / 2);
            velocity = new Vector2(shiftX, shiftY);
        }
    }


}
