﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using System.Runtime.InteropServices;
using CSCore;
using CSCore.Codecs;
using OpenTK;
using RLNET;

namespace Pirate_Rogue.Globals
{
        //TODO: made those as blackboard items
    public static class RandomHelpers
    {
        public static readonly Random Random = new Random(0);
    }

    public static class ArrayHelpers
    {
        //http://stackoverflow.com/questions/25311361/copy-array-to-struct-array-as-fast-as-possible-in-c-sharp
        public static byte[] ToByteArray<T>(T[] source) where T : struct
        {
            GCHandle handle = GCHandle.Alloc(source, GCHandleType.Pinned);
            try
            {
                IntPtr pointer = handle.AddrOfPinnedObject();
                byte[] destination = new byte[source.Length * Marshal.SizeOf(typeof(T))];
                Marshal.Copy(pointer, destination, 0, destination.Length);
                return destination;
            }
            finally
            {
                if (handle.IsAllocated)
                    handle.Free();
            }
        }

        public static T[] FromByteArray<T>(byte[] source) where T : struct
        {
            T[] destination = new T[source.Length / Marshal.SizeOf(typeof(T))];
            GCHandle handle = GCHandle.Alloc(destination, GCHandleType.Pinned);
            try
            {
                IntPtr pointer = handle.AddrOfPinnedObject();
                Marshal.Copy(source, 0, pointer, source.Length);
                return destination;
            }
            finally
            {
                if (handle.IsAllocated)
                    handle.Free();
            }
        }
    }

    public static class MathHelpers
        {
            public static Vector2 AngularVectorForGivenDegrees(float angle)
            {
                return AngularVectorForGivenRadians(Globals.ReadOnlyValues.RadiansInDegrees* angle);
            }
            public static Vector2 AngularVectorForGivenRadians(double radians)
            {
                return new Vector2((float)Math.Sin(radians), -(float)Math.Cos(radians));
            }
            public static void RotateRectangleCornersByCenter(ref Vector2 topLeft, ref Vector2 topRight,
                ref Vector2 downLeft, ref Vector2 downRight, float angle, float recWidth = -1, float recHeight =-1)
            {
                if (recWidth == -1)
                {
                    recWidth = downRight.X - downLeft.X;
                }
                if (recHeight == -1)
                {
                    recHeight = downRight.Y - topRight.Y;
                }
                float halfWidth = recWidth/2f;
                float halfHeight = recHeight/2f;
                // translate point to origin
                float cx = downLeft.X + halfWidth;
                float cy = topLeft.Y + halfHeight;

                RotatePointAroundCenter(ref topLeft, cx,cy,angle);
                RotatePointAroundCenter(ref topRight, cx, cy, angle);
                RotatePointAroundCenter(ref downLeft, cx, cy, angle);
                RotatePointAroundCenter(ref downRight, cx, cy, angle);
            }
            public static void RotatePointAroundCenter(ref Vector2 point, float cx, float cy, float angle)
            {
                float tempX = point.X - cx;
                float tempY = point.Y - cy;

                // now apply rotation
                float rotatedX = (float)(tempX * Math.Cos(angle) - tempY * Math.Sin(angle));
                float rotatedY = (float)(tempX * Math.Sin(angle) + tempY * Math.Cos(angle));

                // translate back
                point.X = rotatedX + cx;
                point.Y = rotatedY + cy;
            }

            public static void RotateModel(ref RLDynamicObject model, float angle)
            {
                //http://stackoverflow.com/questions/6207480/how-to-rotate-a-two-dimensional-array-to-an-arbitrary-degree/6207833#6207833
                double cos = Math.Cos((Math.PI / 180) * angle);
                double sin = Math.Sin((Math.PI / 180) * angle);
                int srcWidth = model.renderedCells.GetLength(0);
                int srcHeight = model.renderedCells.GetLength(1);
                int len = srcWidth > srcHeight ? srcWidth : srcHeight;
                int halfLen = len / 2;
                int x0 = (int)(srcWidth / 2 - cos * halfLen - sin * halfLen);
                int y0 = (int)(srcHeight / 2 - cos * halfLen + sin * halfLen);
                int srcX, srcY;
                model.rotatedCells = new RLCell[len, len];
                for (int y = 0; y < len; y++)
                {
                    for (int x = 0; x < len; x++)
                    {
                        srcX = (int)(cos * x + sin * y + x0);
                        srcY = (int)(-sin * x + cos * y + y0);
                        if (srcX >= 0 && srcY >= 0 && x < len && y < len && srcX < srcWidth && srcY < srcHeight)
                            model.rotatedCells[x, y] = model.renderedCells[srcX, srcY];
                    }
                }
            }
        }

        public static class EnumHelper
        {
            public static int GetPositionOfEnumValue(Enum e)
            {
                Array a = Enum.GetValues(e.GetType());
                for(int i=0;i<a.Length;i++)
                {
                    if ((e.ToString()).Equals(a.GetValue(i).ToString()))
                        return i;
                }
                return -1;
            }
        }

        public static class SoundHelpers
        {
            public static IWaveSource CreateAWaveSourceFromFile(string filename)
            {
                try
                {
                    return CodecFactory.Instance.GetCodec(filename);
                }
                catch (Exception)
                {
                }
                return null;
            }
        }



    }

