﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using System.IO;

namespace Pirate_Rogue.Globals
{


    public static class FilePaths
    {
        public static readonly char DEL = Path.DirectorySeparatorChar;
        public static string RES_PATH = "res";
        public static string SAVE_PATH = "playerFiles";

        public static string GetImagesDirPath()
        {
            return RES_PATH + DEL + "images";
        }

        public static string GetSFXDirPath()
        {
            return RES_PATH + DEL + "sfx";
        }
        public static string GetShadersDirPath()
        {
            return RES_PATH + DEL + "shaders";
        }
        public static string GetFontBitmapPath()
        {
            return RES_PATH + DEL + "data" + DEL + "fonts" + DEL + "cp437_8x8.png";
        }

        public static string GetCreditsFilePath()
        {
            return RES_PATH + DEL + "credits.txt";
        }
        public static string GetLogoFilePath()
        {
            return GetImagesDirPath() + DEL + "dagondevlogo.txt";
        }
        public static string GetControlsConfigFilePath()
        {
            return SAVE_PATH + DEL + "controls.cfg";
        }
        public static string GetLogoBackgroundPath()
        {
            return GetImagesDirPath() + DEL + "logo-background.xp";
        }
        public static string GetShipModelPath()
        {
            return GetImagesDirPath() + DEL + "raft.xp";
        }
        public static string GetPlayerModelPath()
        {
            return GetImagesDirPath() + DEL + "human.xp";
        }
        public static string GetTerrainModelPath()
        {
            return GetImagesDirPath() + DEL + "basic_ocean.xp";
        }
        public static string GetSailsModelPath()
        {
            return GetImagesDirPath() + DEL + "sails_0.xp";
        }

        public static string GetWaterSoundsPath()
        {
            return GetSFXDirPath() + DEL + "light_waves_coming_in.mp3";
        }
        public static string GetCreekSound1Path()
        {
            return GetSFXDirPath() + DEL + "wood_boat_creaking1.mp3";
        }
        public static string GetCreekSound2Path()
        {
            return GetSFXDirPath() + DEL + "wood_boat_creaking2.mp3";
        }
        public static string GetCreekSound3Path()
        {
            return GetSFXDirPath() + DEL + "wood_boat_creaking3.mp3";
        }
        public static string GetCreekSound4Path()
        {
            return GetSFXDirPath() + DEL + "wood_boat_creaking4.mp3";
        }

        public static string GetGlyphVertexShaderPath()
        {
            return GetShadersDirPath() + DEL + "glyphs.vert";
        }
        public static string GetGlyphFragmentShaderPath()
        {
            return GetShadersDirPath() + DEL + "glyphs.frag";
        }
        public static string GetGlyphMinVertexShaderPath()
        {
            return GetShadersDirPath() + DEL + "glyphs_min.vert";
        }
        public static string GetGlyphMinFragmentShaderPath()
        {
            return GetShadersDirPath() + DEL + "glyphs_min.frag";
        }
        public static string GetWaterMinVertexShaderPath()
        {
            return GetShadersDirPath() + DEL + "water_min.vert";
        }
        public static string GetWaterMinFragmentShaderPath()
        {
            return GetShadersDirPath() + DEL + "water_min.frag";
        }
    }
}
