﻿attribute vec3 backColor;
varying vec3 backColorVar;

void main()
{
	gl_Position = ftransform();
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_FrontColor = gl_Color;
	backColorVar=backColor;
}