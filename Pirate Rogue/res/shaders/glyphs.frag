﻿uniform sampler2D mainTex;
varying vec3 backColorVar;

vec3 transparent2=vec3(0.8,0.,1.);
vec3 transparent=vec3(1.,0.,1.);

void main()
{
	vec4 textr=texture2D(mainTex, gl_TexCoord[0].st);

	if(textr.r==0.0)
	{	
		if(backColorVar!=transparent&&backColorVar!=transparent2) {
			gl_FragColor=vec4(backColorVar,1.0);
		} else {
			gl_FragColor.a=0.0;
		}

	} else {
			gl_FragColor = gl_Color;		
	}

}