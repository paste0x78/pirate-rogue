﻿attribute vec4 backColor;
varying vec4 backColorVar;

void main()
{
	gl_Position = ftransform();
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_FrontColor = gl_Color;
	backColorVar=backColor;//vec4(1.0,1.0,1.0,1.0);
}