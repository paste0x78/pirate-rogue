﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using Artemis;
using Artemis.Interface;
using OpenTK;

namespace Pirate_Rogue.Components
{
    public class CTransform : ComponentPoolable
    {
        public Vector2 Position;
        private float angle;

        public float Angle
        {
            get { return angle; }
            set
            {
                LastAngle = angle;
                angle = value;
                AngleChanged = true;
            }
        }

        public float LastAngle { get; private set; }
        public bool AngleChanged;

        public CTransform()
        {
            Position=Vector2.Zero;
            Angle = 0;
            AngleChanged = true;
        }
        public CTransform(float x, float y)
        {
            Position=new Vector2(x,y);
        }

        public CTransform(Vector2 pos)
        {
            Position = pos;
        }
        public override void CleanUp()
        {
            Position = Vector2.Zero;
            Angle = 0;
            base.CleanUp();
        }
    }
}
