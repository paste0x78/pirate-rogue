﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using Artemis.Interface;
using OpenTK;
using RLNET;

namespace Pirate_Rogue.Components
{
    class CTile : IComponent
    {
        public RLDynamicObject Model;
        public int Width;
        public int Height;
        public int OffsetX;
        public int OffsetY;

        public CTile(RLDynamicObject model)
        {
            this.Model = model;
            Width = Model.renderedCells.GetLength(0);
            Height = Model.renderedCells.GetLength(1);
        }
        public CTile(RLDynamicObject model, int offsetX, int offsetY) : this(model)
        {
            OffsetX = offsetX;
            OffsetY = offsetY;
        }
    }
}
