﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using Artemis.Interface;
using OpenTK;
using RLNET;

namespace Pirate_Rogue.Components
{
    class CAnimatedTile : IComponent
    {
        public List<RLDynamicObject> Models;
        public int Width;
        public int Height;
        public int Layer;
        public float DeltaPosition;
        public int CurrentFrame;
        public float SecondsBetweenFrames;
        public float FrameTimer;

        public CAnimatedTile(List<RLDynamicObject> models, int layer)
        {
            //TODO: get global random
            Random random = new Random();
            this.Models = models;
            Width = Models[0].renderedCells.GetLength(0);
            Height = Models[0].renderedCells.GetLength(1);
            Layer = layer;
            double d = random.NextDouble();
            SecondsBetweenFrames = 2f;//(float) (random.Next(4,6)*(d<0.5?0.5:d));
            CurrentFrame = 0; //random.Next(0,Models.Count-1);
        }
    }
}
