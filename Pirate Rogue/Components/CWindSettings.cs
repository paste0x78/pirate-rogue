﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using Artemis.Interface;
using System.Collections.Generic;
using OpenTK;
using RLNET;

namespace Pirate_Rogue.Components
{
    public class CWindAndOceanSettings : IComponent
    {
        public CWindAndOceanSettings(float angle, float knots, RLColor oceanColor)
        {
            Angle = angle;
            Knots = knots;
            OceanColor = oceanColor;
            OceanPosition=Vector2.Zero;
        }
        public float Angle;
        public float Knots;
        public RLColor OceanColor;
        public Vector2 OceanPosition;
    }
}
