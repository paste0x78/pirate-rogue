﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Artemis.Interface;

namespace Pirate_Rogue.Components
{
    class CSun : IComponent
    {
        public float SunValue;

        public CSun(float sunValue)
        {
            SunValue = sunValue;
        }
    }
}
