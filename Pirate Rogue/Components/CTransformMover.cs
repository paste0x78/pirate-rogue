﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using Artemis.Interface;
using OpenTK;

namespace Pirate_Rogue.Components
{
    public class CTransformMover : IComponent
    {
        public Vector2 Direction;
        public float Timer;
        public bool Waiting;
        public float Seconds;
        public float WaitTime;

        public CTransformMover(Vector2 dir, float seconds, float waitTime)
        {
            Direction = dir;
            Seconds = seconds;
            WaitTime = waitTime;
        }
    }
}
