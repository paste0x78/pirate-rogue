﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using Artemis.Interface;
using OpenTK;
using RLNET;
using System.Collections.Generic;

namespace Pirate_Rogue.Components
{
    public class CSailsAnimation : IComponent
    {
        public Dictionary<int, List<RLDynamicObject>> framesDictionary;
        public int CurrentAnimation;
        public int CurrentFrame;
        public float SecondsBetweenFrames;
        public float Timer;
        public Vector2 PositionOffset;

        public CSailsAnimation(float secondsBetweenFrames, List<RLDynamicObject>[] frames, Vector2 positionOffset = default(Vector2))
        {
            Timer = 0;
            SecondsBetweenFrames = secondsBetweenFrames;
            framesDictionary = new Dictionary<int, List<RLDynamicObject>>();
            foreach (var fr in frames)
            {
                   framesDictionary.Add(framesDictionary.Count,fr);
            }
            PositionOffset = positionOffset;
        }
    }
}
