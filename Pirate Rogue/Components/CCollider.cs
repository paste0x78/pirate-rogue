﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using Artemis.Interface;
using OpenTK;
using RLNET;

namespace Pirate_Rogue.Components
{
    class CCollider : IComponent
    {
        public RLCollision Collision;
        public Dictionary<Vector2, int> CollisionDictionary;  
        public CCollider(RLCollision collision)
        {
            Collision = collision;
            CollisionDictionary = new Dictionary<Vector2, int>();
            int width = Collision.MapWidth;
            int height = Collision.MapHeight;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    int val = collision.CollisionFlagsMap[x, y];
                    if(val<=0)
                        continue;
                    CollisionDictionary.Add(new Vector2(x,y),val);
                }
            }
        }
    }
}
