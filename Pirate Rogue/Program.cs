﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/

using RLNET;
using NLog;
using Pirate_Rogue.Globals;
using Pirate_Rogue.Singletones;

namespace Pirate_Rogue
{
    public class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        private static int SCREEN_WIDTH = 800;
        private static int SCREEN_HEIGHT = 600;
        private static readonly float BASIC_ZOOM=1f;

        private static readonly int HALF_WIDTH = SCREEN_WIDTH / 2;
        private static readonly int HALF_HEIGHT = SCREEN_HEIGHT / 2;

        private static RLRootConsole rootConsole;

        private static GameState gameState;
        private static MainMenuState _mainMenuState;

        private static Globals.GameState currentGameState;

        public static void Main()
        {
            //comment this line if you don't want to use file watcher and want to load files from project res folder
//basePath + "res");
            string basePath = System.Reflection.Assembly.GetEntryAssembly().Location;
            string[] splitPath = basePath.Split(FilePaths.DEL);
            basePath = basePath.Substring(0, basePath.Length - splitPath[splitPath.Length - 1].Length);
            FileWatcher.Initialize(basePath + "res");//"..//..//res");
            logger.Debug("Base path for files: "+ basePath);

            
            StartConsole();

            rootConsole.Update += OnRootConsoleUpdate;
            rootConsole.Render += OnRootConsoleRender;

            LoadMainMenu();
            LoadGameState();

            currentGameState = Globals.GameState.MainMenu;

            rootConsole.Run(60);
            logger.Debug("Game ended.");
            gameState.DisposeArtemisIfExists();
            AudioPlayer.Dispose();
        }
        private static void StartConsole()
        {
            string consoleTitle = "Pirate Rogue";
            int charSize = 8;

            //TODO: add reloading of font bitmap
            rootConsole = new RLRootConsole(Globals.FilePaths.GetFontBitmapPath(), //FONT_ALPHA_PATH, 
                SCREEN_WIDTH, SCREEN_HEIGHT, charSize, charSize, System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphVertexShaderPath()),
                            System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphFragmentShaderPath()), BASIC_ZOOM, consoleTitle);
            rootConsole.InitGameScreen(System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphMinVertexShaderPath()),
                            System.IO.File.ReadAllText(Globals.FilePaths.GetGlyphMinFragmentShaderPath()),false,
                            Globals.ChangableValues.PerformanceSettings.MaximumZoomOut);
            logger.Debug("Game started.");
            
        }


        private static void LoadMainMenu()
        {
            _mainMenuState = new MainMenuState();
        }

        private static void LoadGameState()
        {
            gameState = new GameState(rootConsole);
        }
        private static void OnRootConsoleUpdate(object sender, UpdateEventArgs e)
        {
            FileWatcher.Update();
            if (FileWatcher.NeedToReset)
            {
                logger.Debug("FileWatcher found changes, reloading game...");
                gameState.Reset(rootConsole);
            }
            if (gameState.Initialized)
            {

                switch (currentGameState)
                {
                    case Globals.GameState.InTheGame:
                        currentGameState = gameState.OnRootConsoleUpdate(rootConsole, currentGameState, e.Time);
                        break;
                    case Globals.GameState.MainMenu:
                    case Globals.GameState.PauseMenu:
                        currentGameState=_mainMenuState.OnRootConsoleUpdate(rootConsole, currentGameState);
                        break;
                }

            }

        }
        private static void OnRootConsoleRender(object sender, UpdateEventArgs e)
        {
            if (gameState.Initialized)
            {
                switch (currentGameState)
                {
                    case Globals.GameState.InTheGame:
                        gameState.OnRootConsoleRender(rootConsole,currentGameState);
                        break;
                    case Globals.GameState.MainMenu:
                    case Globals.GameState.PauseMenu:
                        _mainMenuState.OnRootConsoleRender(rootConsole,currentGameState,rootConsole.DeltaInSeconds);
                        break;
                }

            }
        }
            

    }

}
