﻿/*
    Copyright 2016, Maciej 'DagonDev' Szewczyk

    This file is part of Pirate Rogue.

    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Pirate Rogue. If not, see<http://www.gnu.org/licenses/>.
*/
using Artemis;
using Pirate_Rogue.Components;
using RexTools;
using RLNET;
using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Pirate_Rogue.Singletones;
using RogueSharp;
using Pirate_Rogue.Components.Flags;

namespace Pirate_Rogue.Templates
{
    public static class EntityCreator
    {
        public static void CreateSunSettings(EntityWorld entityWorld, RLRootConsole rootConsole, string sunTag)
        {
            Entity e = entityWorld.CreateEntity();
            e.AddComponent(new CSun(1.5f));
            e.Tag = sunTag;
            e.Refresh();
        }
        public static void CreateWaterSounds(EntityWorld entityWorld, RLRootConsole rootConsole, string filePath)
        {
            Entity e = entityWorld.CreateEntity();
            e.AddComponent(new CSound(Globals.SoundHelpers.CreateAWaveSourceFromFile(filePath)));
            e.Refresh();
        }
        public static void CreateWindSettings(EntityWorld entityWorld, RLRootConsole rootConsole, string windTag)
        {
            Entity e = entityWorld.CreateEntity();
            e.AddComponent(new CWindAndOceanSettings(0f, 1.6f, new RLColor(43, 140, 121)));
            e.Tag = windTag;
            e.Refresh();
        }

        public static void CreateFPSCounter(EntityWorld entityWorld)
        {
            Entity e = entityWorld.CreateEntity();
            e.AddComponent<CFpsCounter>(new CFpsCounter());
            e.Refresh();
        }
        public static void CreateWindRaycast(EntityWorld entityWorld, RLRootConsole rootConsole,Vector2 startPoint, float directionAngle, float distanceToTravel)
        {
            Entity e = entityWorld.CreateEntity();
            CWindRaycast windRaycast=e.AddComponentFromPool<CWindRaycast>();
            windRaycast.StartPoint = startPoint;
            windRaycast.Angle = directionAngle;
            windRaycast.EndPoint = startPoint +distanceToTravel*Globals.MathHelpers.AngularVectorForGivenDegrees(directionAngle);
            e.Refresh();
        }
        public static void CreateShip(EntityWorld entityWorld, RLRootConsole rootConsole)
        {
            Entity e = entityWorld.CreateEntity();

            /*            RLDynamicObject RLDynamicObject = null;
                        RexReaderHelper.LoadXP(SHIP_PATH, false, 1, ref RLDynamicObject);
                        List<RLDynamicObject> frames = RexReaderHelper.CutFramesFromModel(RLDynamicObject, 20, 20);
                        foreach (RLDynamicObject frame in frames)
                        {
                            rootConsole.CreateDynamicObjectBuffers(frame);
                        }
                        e.AddComponent(new CStaticAnimation("idle", 1f, frames));*/

            RLDynamicObject windCollider = null;
            RexReaderHelper.LoadXP(Globals.FilePaths.GetShipModelPath(), false, 1, ref windCollider);
            windCollider = RexReaderHelper.CutFramesFromModel(windCollider, 20, 20)[0];
            rootConsole.CreateDynamicObjectBuffers(windCollider);
            CWindCollider cWindCollider = new CWindCollider(windCollider);
            e.AddComponent<CWindCollider>(cWindCollider);

            /*            RLDynamicObject foam = null;
                        RexReaderHelper.LoadXP(Globals.FilePaths.GetShipModelPath(), false, 2, ref foam);
                        List<RLDynamicObject> frames2 = RexReaderHelper.CutFramesFromModel(foam, 20, 20);

                        rootConsole.CreateDynamicObjectBuffers(frames2);
                        e.AddComponent(new CStaticAnimation("foam",0.4f,frames2));*/

            RLDynamicObject rlDynamicObject = null;
            RexReaderHelper.LoadXP(Globals.FilePaths.GetShipModelPath(), false, 0, ref rlDynamicObject);
            List<RLDynamicObject> frames = RexReaderHelper.CutFramesFromModel(rlDynamicObject, 20, 20);

            rootConsole.CreateDynamicObjectBuffers(frames[0]);
            e.AddComponent(new CModel(frames[0]));

            rlDynamicObject = null;
            RexReaderHelper.LoadXP(Globals.FilePaths.GetSailsModelPath(), false, 0, ref rlDynamicObject);
            List<RLDynamicObject> noWindFrames = RexReaderHelper.CutFramesFromModel(rlDynamicObject, 20, 20, 0);
            rootConsole.CreateDynamicObjectBuffers(noWindFrames);
            List<RLDynamicObject> mediumWindFrames = RexReaderHelper.CutFramesFromModel(rlDynamicObject, 20, 20, 1);
            rootConsole.CreateDynamicObjectBuffers(mediumWindFrames);
            List<RLDynamicObject> strongWindFrames = RexReaderHelper.CutFramesFromModel(rlDynamicObject, 20, 20, 2);
            rootConsole.CreateDynamicObjectBuffers(strongWindFrames);
            CSailsAnimation sailsAnimation = new CSailsAnimation(0.15f, new List<RLDynamicObject>[] { noWindFrames, mediumWindFrames, strongWindFrames });
            e.AddComponent<CSailsAnimation>(sailsAnimation);


            CTransform cTransform = e.AddComponentFromPool<CTransform>();
            cTransform.Position = new Vector2(40, 30);
            cTransform.Angle = 180;
            e.AddComponent(cTransform);
            //TODO: make poolable
            e.AddComponent<CVelocity>(new CVelocity());
            e.AddComponent<CIsInWater>(new CIsInWater());

            e.Refresh();
        }
        public static void CreatePlayer(EntityWorld entityWorld, RLRootConsole rootConsole, string playerTag)
        {
            Entity e = entityWorld.CreateEntity();
            e.Tag = playerTag;

            CTransform cTransform = e.AddComponentFromPool<CTransform>();
            cTransform.Position = new Vector2(45,40);
            cTransform.Angle = 180;
            e.AddComponent<CVelocity>(new CVelocity());

            RLDynamicObject rlDynamicObject = null;
            RexReaderHelper.LoadXP(Globals.FilePaths.GetPlayerModelPath(), false, 0, ref rlDynamicObject);
            rootConsole.CreateDynamicObjectBuffers(rlDynamicObject);
            rlDynamicObject.CreateUnalteredModel();

            e.AddComponent(new CModel(rlDynamicObject));

            rlDynamicObject = null;
            RexReaderHelper.LoadXP(Globals.FilePaths.GetPlayerModelPath(), false, 1, ref rlDynamicObject);
            int width = rlDynamicObject.renderedCells.GetLength(0);
            int height =
            rlDynamicObject.renderedCells.GetLength(1);
            RLCollision collision = new RLCollision(width, height);
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    collision.CollisionFlagsMap[x, y] = (rlDynamicObject.renderedCells[x, y].backColor !=
                                                         Globals.ReadOnlyValues.TransparentColor || rlDynamicObject.renderedCells[x, y].backColor !=
                                                         Globals.ReadOnlyValues.TransparentColor2
                        ? 1
                        : 0);
                }
            }
            e.AddComponent<CCollider>(new CCollider(collision));
            e.Refresh();
        }

    }

}
