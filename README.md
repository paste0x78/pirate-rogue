# [ Pirate Rogue ](https://www.reddit.com/r/piraterogue)

## State of project

#### Current Build: **b02**

#### [Download binaries](https://bitbucket.org/dagondev/pirate-rogue/downloads)

#### Naming Convention
b**XY***abc* - where **X** is number of version, **Y** number of milestone, *abc*, not required, iteration number over current milestone

[Link to list of versions and interations](https://www.reddit.com/r/piraterogue/comments/4aoesp/roadmap/)

## Roadmap

**[Roadmap reddit thread](https://www.reddit.com/r/piraterogue/comments/4aoesp/roadmap/)**

## Tech

* .NET Framework **v4.5**
* OpenGL **v2.1.0**

Pirate Rogue uses a number of open source projects to work properly:

* [ RLNet DagonDev Fork Repo ](https://bitbucket.org/dagondev/rlnet/)
* * From 2016-03-08 you need to **clone and compile my fork.** Link is above, but **you can`t use NuGET for that.**
* [ RexReader DagonDev Fork Repo ](https://github.com/BaconSoap/RexReader)
* * From 2016-03-17 you need to **clone and compile my fork.** Link is above, but **you can`t use NuGET for that.**
* [ NLOG ](http://nlog-project.org/)
* [ CSCore ](https://github.com/filoe/cscore)
* [ Artemis entity system ](https://thelinuxlich.github.io/artemis_CSharp/)

## Building project

* Windows

    1. Install [ Visual Studio 2015 Community version ](https://www.visualstudio.com/en-us/products/visual-studio-community-vs.aspx) or your favourite IDE of choice.
    2. Clone fork of RLNET and RexReader in same directory where pirate-rogue folder repo us. Example:
        ```mkdir PirateRogue
            git clone https://bitbucket.org/dagondev/pirate-rogue
            cd ..
            git clone https://bitbucket.org/dagondev/rlnet/ 
    		git clone https://github.com/dagondev/RexReader```
    3. Run provided project file **Pirate Rogue.sln** located in pirate-rogue folder.
    4. Right click on project in Solution Explorer and select **Manage NuGET Packages**.
    5. Install/update all required libraries.
    6. Copy all required game assets to the res folder from current binary release, check chapter **Game assets** for more information.
    7. Build it and run it via Start button or use F5 key.

* Linux
	
	**Tested on Fedora 23. Some slight variation on this should work on other distros** 
    1. Install Mono and Monodevelop
    
        Follow the instructions for your distro over [here](http://www.mono-project.com/docs/getting-started/install/linux/)
    
    2. Clone PirateRogue and the fork of RLNET and RexReader.
    
        Both of these repos need to be under under the same folder.
    
        ``` 
        mkdir PirateRogue
        git clone https://bitbucket.org/dagondev/pirate-rogue
        cd ..
        git clone https://bitbucket.org/dagondev/rlnet/ 
		git clone https://github.com/dagondev/RexReader```

    3. Open the project in Monodevelop
    
        You should see a file called ```Pirate Rogue.sln``` in the main repo. Open that.

    4. Install the required libraries
    
        Project > update NuGet packages

    5. Copy all required game assets to the res folder from current binary release, check chapter **Game assets** for more information.

    6. Built it

        Build > Build All (or press f8)

    7. Run it
    
        Run > start debugging (or press f5)

        That's it! Have fun slowly spinning your ship in circles!
	
	*Credit goes to **MolarAmbiguity** for this guide.*
        

## Development

If you really like this project and think you can greatly contribute to Pirate Rogue send me PM at reddit. 

You are always free to fork repo and do pull request if you think your change is important. I am always eager to hear your ideas or feedback at **[ Pirate Rogue reddit ](https://www.reddit.com/r/piraterogue/)**

### Development Basics

##### Getting started

1. **MOST IMPORTANT STEP** Understand how ECS work and get basic knowledge on using [ Artemis CSharp ](https://thelinuxlich.github.io/artemis_CSharp/)
2. Look at Program class to see how game is started and check GameState class to see how game loop looks like.
3. From there focus on specific thing you would like to change, and look what systems/components are initialized in GameState class, by checking **StartArtemisFramework()** method.

#### Tips

* To convert angles to radians or ticks to seconds, multiply by values from **Globals.Values** class.
* To get delta time from last draw/update loop use:
* * In ticks: **entityWorld.Delta**
* * In seconds: **rootConsole.DeltaInSecs**
* To get registered singletones use **EntitySystem.BlackBoard.GetEntry<Type>(Type.ToString())** method.
* **Begin()** method of artemis systems is called every frame (although you need to call **EntitySystem.BlackBoard.GetEntry<Type>(Type.ToString())** there!), so to instantiate something use constructor instead.

#### Game assets

Only ASCII graphics are on the public repository, thanks to their small size and a lot of iterations over them. Rest of the assets (like music/sounds) you need to get from latest binary package to your **"Pirate Rogue\res"** folder.
Simplest way would be copy whole content of **"res"** folder from binary except "images" folder.

##### ASCII graphics

Graphics are created with [RexPaint](http://www.gridsagegames.com/rexpaint/) program. 
Idea is to have [RexPaint](http://www.gridsagegames.com/rexpaint/) extracted into "Pirate Rogue\res" which allows for easy editing all of created files and quick iteration (file watcher reloads game whenever any graphic changes).

###### Transparency

* **Transparent color** is defined by RGB == (1f,0f,1f) == (255,0,255) or (0.5f,0f,1f) == (127,0,255) and should be used as background color
* * The first one is default transparent color used in REXPaint which means you won't see stuff in the editor either, doesn't matter what glyph you select.
* * Second one is custom transparent color used only in game. It's purpose is to allow having semi transparent/filled glyphs, which makes background with that color have its black pixels removed from rendered image

#### Screen Orientation

* **X axis from left to right**
* **Y axis from top to bottom**

(Default OpenTK screen orientation)

#### Model orientation

* Model **position** variable states **top-left corner** of the model.
* * (Default OpenTK screen orientation)
* Model **default angle** is **0** and that translates to **facing top side**.
* * Angle of 90 means facing **right side.**
 
#### Units

**Unless stated otherwise...**

* Every **position** vector unit is in **cells** 
* Every **angle** unit is in **degrees**
* Every **time** unit is in **ticks** 

#### OpenGL

##### Best practices

TODO 

##### OpenGL 2.1 vs 4.x

If you write in shader something like ```gl_Color.rgb=someVec3;``` this will work in OpenGL 2.1, but not in OpenGL 4.0,
instead you need assign this as vec4 like ```gl_Color = vec4(someVec3,1.0);```

#### Entity Component System Paradigm

This project use [ Artemis CSharp ](https://thelinuxlich.github.io/artemis_CSharp/) which is C# implementation of Artemis library, created in Java.

**Follow the tutorial: [link](https://thelinuxlich.github.io/artemis_CSharp/) and links provided there.**

I am not going to tutor you how to use ECS, but here are the basics: (abstract from specific ECS implementation)

*In contrast to the Object Oriented Paradigm (OOP), instead of focusing on inheritance (each class represents 'type of object' - you start with basic/abstract one and by inheriting over you get more specific: GameObject -> Movable -> Vehicle -> Car), ECS focuses on composition.*

*Data is stored in **Components**, logic based on data is processed in **Systems** by iterating over **Entities** that have required components, and 'type of object' is represented by **Entities** that are nothing more than unique ID with array of instantiated components. Inheritance is avoided and each 'feature'/'dataset'/'type' is represented by creating new Components/System focusing only on **one** distinct thing, inastead by inhertiting base classes.*

In other words:

From [Wikipedia page](https://en.wikipedia.org/wiki/Entity_component_system)

**Entity**: The entity is a general purpose object. Usually, it only consists of a unique id. They "tag every coarse gameobject as a separate item". Implementations typically use a plain integer for this.

**Component**: the raw data for one aspect of the object, and how it interacts with the world. "Labels the Entity as possessing this particular aspect". Implementations typically use Structs, Classes, or Associative Arrays.

**System**: Each System runs continuously (as though each System had its own private thread) and performs global actions on every Entity that possesses a Component of the same aspect as that System.

## Todos

* Add screenshots/graphics to guide
* [Open Issues on Bitbucket](https://bitbucket.org/dagondev/pirate-rogue/issues?status=new&status=open)


## Copyright 2016, Maciej 'DagonDev' Szewczyk


License
----
    Pirate Rogue is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Pirate Rogue is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.